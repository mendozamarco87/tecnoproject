<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 07/08/2016
 * Time: 15:52
 */
require_once ("../negocio/permisos.php");
require_once ("baseservice.php");


function get($id){
    $n = new permisos();
    $n->setid($id);
    response(200, "success", "Todo Ok", $n->get($id));
}

function getAll(){
    $n = new permisos();
    response(200, "success", "Todo Ok", $n->getAll());
}

function post($json){
    $n = new permisos();
    try {
        $n->setid_dominio($json->id_dominio);
        $n->setip($json->ip);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->insertar()){
        response(200, "success", "El permiso se registro correctamente.", $n);
    }
}

function put($json){
    $n = new permisos();
    try {
        $n->setid($json->id);
        $n->setid_dominio($json->id_dominio);
        $n->setip($json->ip);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->modificar()){
        response(200, "success", "El permiso se Actualizo correctamente.", $n);
    }
}

function delete($id){
    $n = new zona_directa();
    try {
        $n->setid($id);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->eliminar()){
        response(200, "success", "El permiso se eliminio correctamente.");
    }
}