<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 07/08/2016
 * Time: 14:58
 */
require_once ("../negocio/zona_inversa.php");
require_once ("baseservice.php");


function get($id){
    $n = new zona_inversa();
    $n->setid($id);
    response(200, "success", "Todo Ok", $n->get($id));
}

function getAll(){
    $n = new zona_inversa();
    response(200, "success", "Todo Ok", $n->getAll());
}

function post($json){
    $n = new zona_inversa();
    try {
        $n->setid_dominio($json->id_dominio);
        $n->sethost($json->host);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->insertar()){
        response(200, "success", "La Zona inversa se registro correctamente.", $n);
    }
}

function put($json){
    $n = new zona_inversa();
    try {
        $n->setid($json->id);
        $n->setid_dominio($json->id_dominio);
        $n->sethost($json->host);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->modificar()){
        response(200, "success", "La Zona inversa se Actualizo correctamente.", $n);
    }
}

function delete($id){
    $n = new zona_inversa();
    try {
        $n->setid($id);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->eliminar()){
        response(200, "success", "La Zona Inversa se eliminio correctamente.");
    }
}