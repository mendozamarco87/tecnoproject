<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 08/08/2016
 * Time: 14:39
 */
require_once ("../negocio/mail.php");
require_once ("baseservice.php");


function post($json){
    $n = new Mail();
    try {
        $n->numero = $json->numero;
        $n->cliente = $json->cliente;
        $n->fecha = $json->fecha;
        $n->asunto = $json->asunto;
        $n->mensaje = $json->mensaje;
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->insertar()){
        response(200, "success", "El mail se registro correctamente.", $n);
    }
}