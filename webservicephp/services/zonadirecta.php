<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 07/08/2016
 * Time: 11:19
 */
require_once ("../negocio/zona_directa.php");
require_once ("baseservice.php");


function get($id){
    $n = new zona_directa();
    $n->setid($id);
    response(200, "success", "Todo Ok", $n->get($id));
}

function getAll(){
    $n = new zona_directa();
    response(200, "success", "Todo Ok", $n->getAll());
}

function post($json){
    $n = new zona_directa();
    try {
        $n->setid_dominio($json->id_dominio);
        $n->setip($json->ip);
        $n->setcorreo_responsable($json->correo_responsable);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->insertar()){
        response(200, "success", "La Zona directa se registro correctamente.", $n);
    }
}

function put($json){
    $n = new zona_directa();
    try {
        $n->setid($json->id);
        $n->setid_dominio($json->id_dominio);
        $n->setip($json->ip);
        $n->setcorreo_responsable($json->correo_responsable);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->modificar()){
        response(200, "success", "La Zona directa se Actualizo correctamente.", $n);
    }
}

function delete($id){
    $n = new zona_directa();
    try {
        $n->setid($id);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($n->eliminar()){
        response(200, "success", "La Zona directa se eliminio correctamente.");
    }
}