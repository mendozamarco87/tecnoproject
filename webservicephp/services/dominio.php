<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 06/08/2016
 * Time: 2:41
 */
require_once ("../negocio/dominio.php");
require_once ("baseservice.php");

function get($id){
    $dominio = new Dominio();
    $dominio->setid($id);
    response(200, "success", "Todo Ok", $dominio->get($id));
}

function getAll(){
    $dominio = new Dominio();
    response(200, "success", "Todo Ok", $dominio->getAll());
}

function post($json){
    $dominio = new Dominio();
    try {
        $dominio->setnombre($json->nombre);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }
    
    if ($dominio->insertar()){
        response(200, "success", "El dominio se registro correctamente.", $dominio);
    }
}

function put($json){
    $dominio = new Dominio();
    try {
        $dominio->setid($json->id);
        $dominio->setnombre($json->nombre);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($dominio->modificar()){
        response(200, "success", "El dominio se registro correctamente.", $dominio);
    }
}

function delete($id){
    $dominio = new Dominio();
    try {
        $dominio->setid($id);
    } catch (Exception $e) {
        response(422, "error", "Los Datos son incorrectos...");
    }

    if ($dominio->eliminar()){
        response(200, "success", "El dominio se registro correctamente.", $dominio);
    }
}