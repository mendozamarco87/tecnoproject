<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 06/08/2016
 * Time: 2:38
 */


switch ($_SERVER['REQUEST_METHOD']) {
    case "POST":
        $datos = obtenerDatos();
        post($datos);
        break;
    case "GET":
        if (!isset($_GET["id"])) {
            getAll();
            return;
        }
        get($_GET["id"]);
        break;
    case "PUT":
        $datos = obtenerDatos();
        put($datos);
        break;
    case "DELETE":
        if (isset($_GET["id"])) {
            delete($_GET["id"]);
        } else {
            response(422, "error", "Nothing to add. Check json");
        }
        break;
}

function obtenerDatos()
{
    $obj = json_decode(file_get_contents('php://input'));
    $objArr = (array)$obj;
    if (empty($objArr)) {
        response(422, "error", "Nothing to add. Check json");
        return;
    }
    return $obj;
}

function response($code=200, $status="", $message="", $data="") {
    http_response_code($code);
    if( !empty($status) && !empty($message) ){
        $response = array("status" => $status ,"message"=>$message, "data"=>$data);
        echo json_encode($response,JSON_PRETTY_PRINT);
    }
}