<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 07/08/2016
 * Time: 23:50
 */

require_once "../datos/configdb.php";
require_once ("baseservice.php");


function get($dominio){
    $sql = "SELECT z.id,d.id as iddominio ,d.nombre, z.ip, z.correo_responsable FROM zona_directa z, dominio d WHERE z.id_dominio = d.id and d.estado = true and d.nombre = '$dominio'";
    if ($data = execute($sql)){
        $c = $data->RecordCount();
        $arreglo = [];
        for($i = 0; $i < $c; $i++) {
            $arreglo[$i] = $data->GetRowAssoc();
            $data->MoveNext();
        }
        response(200, "success", "Todo Ok", $arreglo);
    }else{
        print_r($data);
    }
}

function getAll(){
    $sql = "SELECT z.id,d.id as iddominio ,d.nombre, z.ip, z.correo_responsable FROM zona_directa z, dominio d WHERE z.id_dominio = d.id and d.estado = true ";
    if ($data = execute($sql)){
        $c = $data->RecordCount();
        $arreglo = [];
        for($i = 0; $i < $c; $i++) {
            $arreglo[$i] = $data->GetRowAssoc();
            $data->MoveNext();
        }
        response(200, "success", "Todo Ok", $arreglo);
    }else{
        print_r($data);
    }
}

function execute($sql){
    $conexion = Conexion::getInstancia();
    try{
        $conexion->conectar();
        $rs = $conexion->ejecutar($sql);
        $conexion->cerrar();
        return $rs;
    } catch (Exception $e) {
        print_r($e->getTraceAsString());
    }
}