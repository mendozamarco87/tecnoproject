<?php
/**
 * @author mendo
 * @version 1.0
 * @created 05-ago.-2016 23:30:51
 */
require_once "../datos/abmbase.php";

class zona_directa extends ABMbase
{
	var $id;
	var $id_dominio;
	var $ip;
	var $correo_responsable;
	var $estado = true;

	function zona_directa()
	{
	}


	function getid()
	{
		return $this->id;
	}

	function setid($newVal)
	{
		$this->id = $newVal;
	}

	function getid_dominio()
	{
		return $this->id_dominio;
	}

	function setid_dominio($newVal)
	{
		$this->id_dominio = $newVal;
	}

	function getip()
	{
		return $this->ip;
	}

	function setip($newVal)
	{
		$this->ip = $newVal;
	}

	function getestado()
	{
		return $this->estado;
	}

	function setestado($newVal)
	{
		$this->estado = $newVal;
	}

	function getcorreo_responsable()
	{
		return $this->correo_responsable;
	}

	function setcorreo_responsable($newVal)
	{
		$this->correo_responsable = $newVal;
	}

	function insertar() {
		$id = self::insert(array(
			"id_dominio"=>$this->id_dominio,
			"ip"=>$this->ip,
			"correo_responsable"=>$this->correo_responsable,
			"estado"=>"true"
		));
		if ($id) {
			$this ->id = $id;
			return true;
		}
		return false;
	}

	function modificar() {
		return self::modify(array(
			"id_dominio"=>$this->id_dominio,
			"ip"=>$this->ip,
			"correo_responsable"=>$this->correo_responsable,
			"WHERE"=>"id = $this->id"
		));
	}

	function eliminar() {
		$this->estado = false;
		return self::modify(array(
			"estado"=>"false",
			"WHERE"=>"id = $this->id"
		));
	}

	function table_name() {
		return "zona_directa";
	}

	function getMisZonaDirectas($iduser){
		$sql = "SELECT z.*,d.nombre as dominio FROM zona_directa z, dominio d WHERE z.id_dominio=d.id and d.id_usuario='$iduser' and z.estado = true";
		if ($data = $this->execute($sql)){
			$c = $data->RecordCount();
			$arreglo = [];
			for($i = 0; $i < $c; $i++) {
				$arreglo[$i] = $data->GetRowAssoc();
				$data->MoveNext();
			}
			return $arreglo;
		}else{
			print_r($data);
		}
	}

}
?>