<?php
/**
 * @author mendo
 * @version 1.0
 * @created 05-ago.-2016 23:30:49
 */
require_once "../datos/abmbase.php";

class Permisos extends ABMbase
{
	var $id;
	var $id_dominio;
	var $ip;
	var $estado = true;

	function Permisos($id=null, $id_dominio=null, $ip=null, $estado=null){
		$this->id = $id;
		$this->id_dominio = $id_dominio;
		$this->ip = $ip;
		$this->estado = $estado;
	}


	function getid()
	{
		return $this->id;
	}

	function setid($newVal)
	{
		$this->id = $newVal;
	}

	function getid_dominio()
	{
		return $this->id_dominio;
	}

	function setid_dominio($newVal)
	{
		$this->id_dominio = $newVal;
	}

	function getip()
	{
		return $this->ip;
	}

	function setip($newVal)
	{
		$this->ip = $newVal;
	}

	function getestado()
	{
		return $this->estado;
	}

	function setestado($newVal)
	{
		$this->estado = $newVal;
	}

	function insertar() {
		$id = self::insert(array(
			"id_dominio"=>$this->id_dominio,
			"ip"=>$this->ip,
			"estado"=>"true"
		));
		if ($id) {
			$this ->id = $id;
			return true;
		}
		return false;
	}

	function modificar() {
		return self::modify(array(
			"id_dominio"=>$this->id_dominio,
			"ip"=>$this->ip,
			"WHERE"=>"id = $this->id"
		));
	}

	function eliminar() {
		$this->estado = false;
		return self::modify(array(
			"estado"=>"false",
			"WHERE"=>"id = $this->id"
		));
	}

	function table_name() {
		return "permisos";
	}

	function getAllPermisos($idUser=null){
		$sql = "SELECT p.*, d.nombre as nombre_dominio FROM dominio as d, permisos as p
				where p.id_dominio = d.id and d.estado = TRUE and p.estado = TRUE and d.id_usuario =".$idUser.";";
		if ($data = $this->execute($sql)){
			$c = $data->RecordCount();
			$arreglo = [];
			for($i = 0; $i < $c; $i++) {
				$arreglo[$i] = $data->GetRowAssoc();
				$data->MoveNext();
			}
			return $arreglo;
		}else{
			print_r($data);
		}
	}

}
?>