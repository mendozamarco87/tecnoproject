<?php

/**
 * @author mendo
 * @version 1.0
 * @updated 05-ago.-2016 23:30:47
 */
require_once "../datos/abmbase.php";

class Dominio extends ABMbase{

	var $id;
	var $id_usuario;
	var $nombre;
	var $estado=true;

	function Dominio($id=null, $id_usuario=null, $nombre=null, $estado=null){
		$this->id = $id;
		$this->id_usuario = $id_usuario;
		$this->nombre = $nombre;
		$this->estado = $estado;
	}

	function getid() {
		return $this->id;
	}

	function setid($newVal) {
        $this->id = $newVal;
	}

	function getIdUsuario() {
		return $this->id_usuario;
	}

	function setIdUsuario($newVal) {
		$this->id_usuario = $newVal;
	}

	function getnombre() {
		return $this->nombre;
	}

	function setnombre($newVal) {
        $this->nombre = $newVal;
	}

	function getestado() {
		return $this->estado;
	}

	function setestado($newVal) {
        $this->estado = $newVal;
	}

	function insertar() {
		$id = self::insert(array(
			"nombre"=>$this->nombre,
			"id_usuario"=>$this->id_usuario,
			"estado"=>"true"
		));
		if ($id) {
			$this->id = $id;
			return true;
		}
		return false;
	}

	function modificar() {
		return self::modify(array(
			"nombre"=>$this->nombre,
			"WHERE"=>"id = $this->id"
		));
	}

	function eliminar() {
		$this->estado = false;
		return self::modify(array(
			"estado"=>"false",
			"WHERE"=>"id = $this->id"
		));
	}

	function table_name() {
		return "dominio";
	}

	function getAllDominio($idUser){
		$sql = "SELECT * FROM " . $this->table_name() . " WHERE estado = True and id_usuario = ".$idUser.";";
		if ($data = $this->execute($sql)){
			$c = $data->RecordCount();
			$arreglo = [];
			for($i = 0; $i < $c; $i++) {
				$arreglo[$i] = $data->GetRowAssoc();
				$data->MoveNext();
			}
			return $arreglo;
		}else{
			print_r($data);
		}
	}

	function getAllReportes($idUser=null){
		$sql = "select '.com' as tipo_dominio,count(*)*100/(select count(*) from dominio
				where estado = TRUE and id_usuario = ".$idUser.") as total
				from dominio where nombre LIKE '%.com' and estado = TRUE and id_usuario = ".$idUser."
				union
				select '.bo' as tipo_dominio,count(*)*100/(select count(*) from dominio
				where estado = TRUE and id_usuario = ".$idUser.") as total
				from dominio where nombre LIKE '%.bo' and estado = TRUE and id_usuario = ".$idUser."
				union
				select '.edu' as tipo_dominio,count(*)*100/(select count(*) from dominio
				where estado = TRUE and id_usuario = ".$idUser.") as total
				from dominio where nombre LIKE '%.edu' and estado = TRUE and id_usuario = ".$idUser."
				union
				select '.org' as tipo_dominio,count(*)*100/(select count(*) from dominio
				where estado = TRUE and id_usuario = ".$idUser.") as total
				from dominio where nombre LIKE '%.org' and estado = TRUE and id_usuario = ".$idUser;
		if ($data = $this->execute($sql)){
			$c = $data->RecordCount();
			$arreglo = [];
			for($i = 0; $i < $c; $i++) {
				$arreglo[$i] = $data->GetRowAssoc();
				$data->MoveNext();
			}
			return $arreglo;
		}else{
			print_r($data);
		}
	}
}

?>
