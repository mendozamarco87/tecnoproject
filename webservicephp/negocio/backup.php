<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 13/08/2016
 * Time: 3:16
 */
require_once "../negocio/dominio.php";
require_once "../negocio/zona_directa.php";
require_once "../negocio/zona_inversa.php";

class backup
{
    var $id_dominio;
    var $dominio;
    var $zona_directa;
    var $zona_inversa;

    function backup($id = null)
    {
        $this->id_dominio = $id;
        $this->dominio = new Dominio();
        $this->zona_directa = new zona_directa();
        $this->zona_inversa = new zona_inversa();
    }

    function generarZonaDirecta($id_zonadirecta){
        try {
            $d = $this->dominio->get($this->id_dominio);
            $zd = $this->zona_directa->get($id_zonadirecta);
            $archivo = "../backup/" . $d["nombre"] . ".zone";
            $this->ZonaDirecta("dns", "A", $zd["ip"], $d["nombre"], $zd["correo_responsable"], $archivo);
            return $archivo;
        } catch (Exception $e) {
            return false;
        }
    }

    function generarZonaInversa($id_zonainversa){
        try {
            $d = $this->dominio->get($this->id_dominio);
            $zi = $this->zona_inversa->get($id_zonainversa);
            $archivo = "../backup/" . $zi["ip"] . ".zone";
            $ip = explode(".", $zi["ip"]);
            $this->ZonaInversa($ip[3], "PTR", $d["nombre"], $zi["correo_responsable"], $archivo);
            return $archivo;
        }catch (Exception $e) {
            return false;
        }
    }


    function ZonaInversa($P1, $P2, $dominio, $responsable, $archivo)
    {
        $encabezado = "\$TTL\t604800" . "\r\n"
            . "@\tIN\tSOA\tdns." . $dominio . ". " . $responsable . ". (" . "\r\n"
            . "\t\t\t015110501\t;Serial" . "\r\n"
            . "\t\t\t3600\t\t;Refresh" . "\r\n"
            . "\t\t\t1800\t\t;Retry" . "\r\n"
            . "\t\t\t604800\t\t;Expire" . "\r\n"
            . "\t\t\t86400 )\t\t;Minimum TTL" . "\r\n"
            . "\tIN\tNS\tdns." . $dominio . ".\r\n";
        $conte=$P1."\tIN\t" . $P2 . "\tdns." . $dominio . ".";
//        $Veri = 0;
//        if (file_exists($archivo)) {
//            $Veri = 1;
//        }
        $DescriptorFichero = fopen($archivo, "w");
        if ($DescriptorFichero == false)
            die("unable to create file");
        fputs($DescriptorFichero, $encabezado);
        fputs($DescriptorFichero, $conte);
        fclose($DescriptorFichero);
    }

    function ZonaDirecta($P1, $P2, $P3, $dominio, $responsable, $archivo)
    {
        $encabezado = "\$TTL\t604800" . "\r\n"
            . "@\tIN\tSOA\tdns." . $dominio . ". " . $responsable . ". (" . "\r\n"
            . "\t\t\t015110501\t;Serial" . "\r\n"
            . "\t\t\t3600\t\t;Refresh" . "\r\n"
            . "\t\t\t1800\t\t;Retry" . "\r\n"
            . "\t\t\t604800\t\t;Expire" . "\r\n"
            . "\t\t\t86400 )\t\t;Minimum TTL" . "\r\n"
            . "@\tIN\tNS\tdns." . $dominio . ".\r\n";
        $conte = $P1."\tIN\t" . $P2 . "\t" . $P3;
//        $Veri = 0;
//        if (file_exists($archivo)) {
//            $Veri = 1;
//        }
        $DescriptorFichero = fopen($archivo, "w");
        if ($DescriptorFichero == false)
            die("unable to create file");
        fputs($DescriptorFichero, $encabezado);
        fputs($DescriptorFichero, $conte);
        fclose($DescriptorFichero);
    }

}
?>