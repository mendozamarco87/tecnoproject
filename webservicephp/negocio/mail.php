<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 08/08/2016
 * Time: 14:33
 */
require_once "../datos/abmbase.php";

class Mail extends ABMbase 
{
    var $numero;
    var $cliente;
    var $fecha;
    var $asunto;
    var $mensaje;

    function insertar() {
        $id = self::insert(array(
            "numero"=>$this->numero,
            "cliente"=>$this->cliente,
            "fecha"=>$this->fecha,
            "asunto"=>$this->asunto,
            "mensaje"=>$this->mensaje
        ));
        if ($id) {
            $this->id = $id;
            return true;
        }
        return false;
    }

    function table_name() {
        return "bitacoramail";
    }
}