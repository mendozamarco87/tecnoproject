<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 12/08/2016
 * Time: 19:58
 */

require_once "../datos/abmbase.php";

class Bitacora extends ABMbase
{
    var $id;
    var $id_usuario;
    var $url;
    var $ip;
    var $fecha;
    var $hora;

    function Bitacora($id=null, $id_usuario=null, $url=null, $ip=null, $fecha=null, $hora=null){
        $this->id = $id;
        $this->id_usuario = $id_usuario;
        $this->url = $url;
        $this->ip = $ip;
        $this->fecha = $fecha;
        $this->hora = $hora;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function getHora()
    {
        return $this->hora;
    }

    public function setHora($hora)
    {
        $this->hora = $hora;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function table_name() {
        return "bitacora";
    }

    function insertar() {
        $id = self::insert(array(
            "id_usuario"=>$this->id_usuario,
            "url"=>$this->url,
            "ip"=>$this->ip,
            "fecha"=>$this->fecha,
            "hora"=>$this->hora
        ));
        if ($id) {
            $this->id = $id;
            return true;
        }
        return false;
    }

    public function getNumeroVisita($idUser){
        $sql = "SELECT COUNT(*) as total_visitas FROM bitacora WHERE id_usuario =". $idUser .";";
        if ($data = $this->execute($sql)){
            $c = $data->RecordCount();
            $arreglo = [];
            for($i = 0; $i < $c; $i++) {
                $arreglo[$i] = $data->GetRowAssoc();
                $data->MoveNext();
            }
            return $arreglo;
        }else{
            print_r($data);
        }
    }

}