<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 08/08/2016
 * Time: 17:49
 */

require_once '../datos/configdb.php';

class Login
{

    private static $conexion;
    var $idUser;
    var $nombres;
    var $idTipo;
    var $nombreTipo;
    var $configTema;

    function veriUser($username, $pw)
    {
        $sql = "select u.id,u.nombre || ' ' || u.app as nombres,u.tipo,tu.nombre, u.config_tema "
            . "from usuario u,tipo_usuario tu "
            . "where u.tipo = tu.id and u.estado=true "
            . "and pass='$pw' and username='$username' limit 1;";
        $data = $this->execute($sql);
        if ($data && $data->RecordCount() == 1) {
            $array = $data->GetRowAssoc();
            $this->idUser = $array['id'];
            $this->nombres = $array['nombres'];
            $this->idTipo = $array['tipo'];
            $this->nombreTipo = $array['nombre'];
            $this->configTema = $array['config_tema'];
            return true;
        } else {
            return false;
        }
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function getNombres()
    {
        return $this->nombres;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function getNombreTipo()
    {
        return $this->nombreTipo;
    }

    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    public function setNombres($nombres)
    {
        $this->nombres = $nombres;
    }

    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;
    }

    public function setNombreTipo($Descripcion)
    {
        $this->nombreTipo = $Descripcion;
    }
    
    public function getConfigTema()
    {
        return $this->configTema;
    }

    private function execute($sql)
    {
        self::$conexion = Conexion::getInstancia();
        try {
            self::$conexion->conectar();
            $rs = self::$conexion->ejecutar($sql);
            self::$conexion->cerrar();
            return $rs;
        } catch (Exception $e) {
            print_r($e->getTraceAsString());
        }
    }
}

?>