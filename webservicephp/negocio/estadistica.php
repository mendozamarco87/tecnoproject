<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 08/08/2016
 * Time: 17:49
 */

require_once "../datos/configdb.php";

class Estadistica {
    
    private static $conexion;

    function getEstadistica(){
        $sql = "select '.com' as tipoDominio, COUNT(*) as total from dominio where nombre LIKE '%.com' union 
                select '.bo' as tipoDominio, COUNT(*) as total from dominio where nombre LIKE '%.bo'
                union
                select '.edu' as tipoDominio, COUNT(*) as total from dominio where nombre LIKE '%.edu'
                union
                select '.es' as tipoDominio, COUNT(*) as total from dominio where nombre LIKE '%.es'";
        if ($data = $this->execute($sql)){
            $c = $data->RecordCount();
            $arreglo = [];
            for($i = 0; $i < $c; $i++) {
                $arreglo[$i] = $data->GetRowAssoc();
                $data->MoveNext();
            }
            return $arreglo;
        }else{
            print_r($data);
        }
    }

    private function execute($sql){
        self::$conexion = Conexion::getInstancia();
        try{
            self::$conexion->conectar();
            $rs = self::$conexion->ejecutar($sql);
            self::$conexion->cerrar();
            return $rs;
        } catch (Exception $e) {
            print_r($e->getTraceAsString());
        }
    }
}

?>