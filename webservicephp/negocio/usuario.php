<?php
require_once "../datos/abmbase.php";

class usuarios extends ABMbase
{
    var $id;
    var $username;
    var $pass;
    var $nombre;
    var $app;
    var $apm;
    var $telefono;
    var $correo;
    var $tipo;
    var $estado = true;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setApp($app)
    {
        $this->app = $app;
    }

    public function setApm($apm)
    {
        $this->apm = $apm;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPass()
    {
        return $this->pass;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getApm()
    {
        return $this->apm;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function actualizarConfig($config){
        return self::modify(array(
            "config_tema" => $config,
            "WHERE" => "id = $this->id"
        ));
    }

    function insertar()
    {
        $id = self::insert(array(
            "username" => $this->username,
            "pass" => $this->pass,
            "nombre" => $this->nombre,
            "app" => $this->app,
            "apm" => $this->apm,
            "telefono" => $this->telefono,
            "correo" => $this->correo,
            "tipo" => $this->tipo,
            "estado" => "true"
        ));
        if ($id) {
            $this->id = $id;
            return true;
        }
        return false;
    }

    function modificar()
    {
        return self::modify(array(
            "nombre" => $this->nombre,
            "app" => $this->app,
            "apm" => $this->apm,
            "telefono" => $this->telefono,
            "correo" => $this->correo,
            "tipo" => $this->tipo,
            "WHERE" => "id = $this->id"
        ));
    }

    function eliminar()
    {
        $this->estado = false;
        return self::modify(array(
            "estado" => "false",
            "WHERE" => "id = $this->id"
        ));
    }

    function table_name()
    {
        return "usuario";
    }

}

?>