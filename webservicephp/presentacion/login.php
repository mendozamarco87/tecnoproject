<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 09/08/2016
 * Time: 22:51
 */

require 'smarty.php';
//require '/usr/share/php/Smarty/Smarty.class.php';
//$smarty = new Smarty;
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // verificamos si ya esta logueado
    if (isset($_SESSION['auth']) && $_SESSION['auth'] == 'y' && !isset($_GET['close'])) {
        header('Location: index.php');
        return;
    } // verificamos si trata cerrar session
    elseif (isset($_GET['close'])) {
        session_destroy();
    }
    $smarty->display('ligth_blue/dark/login.tpl');
    return;
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
// Hacemos la validacion para iniciar session
    if (isset($_POST['user']) && isset($_POST['password'])) {
        include '../negocio/login.php';
        $user = $_POST['user'];
        $password = $_POST['password'];
        $vaUser = new Login();
        if ($vaUser->veriUser($user, $password)) {
            $_SESSION['auth'] = 'y';
            $_SESSION['idUser'] = $vaUser->getIdUser();
            $_SESSION['nombreUser'] = $vaUser->getNombres();
            $_SESSION['idtipoUser'] = $vaUser->getIdTipo();
            $_SESSION['nombreTipoUser'] = $vaUser->getNombreTipo();
            $_SESSION['configTema'] = $vaUser->getConfigTema();
            header('Location: index.php');
            return;
        }
    }
    $smarty->display('ligth_blue/dark/login.tpl');
    return;
}
$smarty->display('ligth_blue/dark/login.tpl');
return;