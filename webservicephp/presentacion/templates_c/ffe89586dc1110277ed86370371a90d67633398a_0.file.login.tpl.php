<?php
/* Smarty version 3.1.30, created on 2016-08-12 01:48:35
  from "C:\xampp\htdocs\tecnoproject\webservicephp\presentacion\templates\ligth_blue\dark\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57ad0ed365f0e0_06347171',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffe89586dc1110277ed86370371a90d67633398a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\tecnoproject\\webservicephp\\presentacion\\templates\\ligth_blue\\dark\\login.tpl',
      1 => 1470959308,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57ad0ed365f0e0_06347171 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- light-blue - v3.3.0 - 2016-03-08 -->

<!DOCTYPE html>
<html>

<!-- Mirrored from demo.flatlogic.com/3.3.1/white/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Aug 2016 21:54:59 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Light Blue - Responsive Admin Dashboard Template</title>

    <link href="templates/ligth_blue/dark/css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <?php echo '<script'; ?>
>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
         chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
         https://code.google.com/p/chromium/issues/detail?id=332189
         */
    <?php echo '</script'; ?>
>
</head>
<body>
<div class="single-widget-container">
    <section class="widget login-widget">
        <header class="text-align-center">
            <h4>Login to your account</h4>
        </header>
        <div class="body">
            <form class="no-margin"
                  action='/tecnoproject/webservicephp/presentacion/validarLogin.php' method="post">
                <fieldset>
                    <div class="form-group">
                        <label for="user">User</label>
                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                            <input name="user" id="user" type="text" class="form-control input-lg"
                                   placeholder="name de user">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>

                        <div class="input-group input-group-lg">
                                    <span class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                            <input name="password" id="password" type="password" class="form-control input-lg"
                                   placeholder="Your Password">
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-block btn-lg btn-danger">
                        <span class="small-circle"><i class="fa fa-caret-right"></i></span>
                        <small>Sign In</small>
                    </button>
                    <a class="forgot" href="#">Forgot Username or Password?</a>
                </div>
            </form>
        </div>
        <footer>
            <div class="facebook-login">
                <a href="index.html"><span><i class="fa fa-facebook-square fa-lg"></i> LogIn with Facebook</span></a>
            </div>
        </footer>
    </section>
</div>
<!-- common libraries. required for every page-->
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/lib/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/lib/jquery-pjax/jquery.pjax.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/lib/widgster/widgster.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/lib/underscore/underscore.js"><?php echo '</script'; ?>
>

<!-- common application js -->
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/js/app.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="templates/ligth_blue/dark/js/settings.js"><?php echo '</script'; ?>
>
</body>

<!-- Mirrored from demo.flatlogic.com/3.3.1/white/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 09 Aug 2016 21:54:59 GMT -->
</html><?php }
}
