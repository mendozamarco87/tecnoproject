<?php
/* Smarty version 3.1.30, created on 2016-08-11 21:46:09
  from "C:\xampp\htdocs\tecnoproject\webservicephp\presentacion\templates\ligth_blue\dark\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57acd601a3db22_49410365',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '69426b8971221967345c67a8dbcc0b3e64a9c076' => 
    array (
      0 => 'C:\\xampp\\htdocs\\tecnoproject\\webservicephp\\presentacion\\templates\\ligth_blue\\dark\\header.tpl',
      1 => 1470944318,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57acd601a3db22_49410365 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Light Blue - Responsive Admin Dashboard Template</title>
    <link href="templates/ligth_blue/dark/css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="logo">
    <h4><a href="index.html">Light <strong>Blue</strong></a></h4>
</div>
<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li class="active">
            <a href="index.html"><i class="fa fa-home"></i> <span class="name">Inicio</span></a>
        </li>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu_array']->value, 'menu');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['menu']->value) {
?>
        <?php if (empty($_smarty_tpl->tpl_vars['menu']->value['submenu'])) {?>
        <li class="active">
            <a href="<?php echo $_smarty_tpl->tpl_vars['menu']->value['accion'];?>
"><i class="fa fa-home"></i> <span class="name"><?php echo $_smarty_tpl->tpl_vars['menu']->value['nombre'];?>
</span></a>
        </li>
        <?php } else { ?>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse"
               data-parent="#side-nav" href="#<?php echo $_smarty_tpl->tpl_vars['menu']->value['id'];?>
"><i class="fa fa-link"></i> <span
                    class="name"><?php echo $_smarty_tpl->tpl_vars['menu']->value['nombre'];?>
</span></a>
            <ul id="<?php echo $_smarty_tpl->tpl_vars['menu']->value['id'];?>
" class="panel-collapse collapse ">
            <?php $_smarty_tpl->_assignInScope('sub_menu_array', $_smarty_tpl->tpl_vars['menu']->value['submenu']);
?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['sub_menu_array']->value, 'sub_menu');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub_menu']->value) {
?>
                <li class=""><a href="<?php echo $_smarty_tpl->tpl_vars['sub_menu']->value['accion'];?>
"><?php echo $_smarty_tpl->tpl_vars['sub_menu']->value['nombre'];?>
</a></li>
            </ul>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </li>
        <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <li class="visible-xs">
            <a href="login.html"><i class="fa fa-sign-out"></i> <span class="name">Sign Out</span></a>
        </li>
    </ul>
</nav>
<div class="wrap">
    <header class="page-header">
        <div class="navbar">
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="visible-phone-landscape">
                    <a href="#" id="search-toggle">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" title="Messages" id="messages"
                       class="dropdown-toggle"
                       data-toggle="dropdown">
                        <i class="glyphicon glyphicon-comment"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" title="8 support tickets"
                       class="dropdown-toggle"
                       data-toggle="dropdown">
                        <i class="glyphicon glyphicon-globe"></i>
                        <span class="count">8</span>
                    </a>
                </li>
                <li class="divider"></li>
                <li class="hidden-xs">
                    <a href="#" id="settings"
                       title="Settings"
                       data-toggle="popover"
                       data-placement="bottom">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li class="hidden-xs dropdown">
                    <a href="#" title="Account" id="account"
                       class="dropdown-toggle"
                       data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" class="dropdown-menu account" role="menu">
                        <li role="presentation" class="account-picture">
                            <img src="img/2.png" alt="">
                            Philip Daineka
                        </li>
                        <li role="presentation">
                            <a href="form_account.html" class="link">
                                <i class="fa fa-user"></i>
                                Profile
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="component_calendar.html" class="link">
                                <i class="fa fa-calendar"></i>
                                Calendar
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#" class="link">
                                <i class="fa fa-inbox"></i>
                                Inbox
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="visible-xs">
                    <a href="#"
                       class="btn-navbar"
                       data-toggle="collapse"
                       data-target=".sidebar"
                       title="">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="hidden-xs"><a href="login.php"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
            <form id="search-form" class="navbar-form pull-right" role="search">
                <input type="search" class="form-control search-query" placeholder="Search...">
            </form>
        </div>
    </header>
    <div class="content container"><?php }
}
