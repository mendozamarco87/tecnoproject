<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 10/08/2016
 * Time: 15:32
 */
session_start();
if (!isset($_SESSION['auth']) || $_SESSION['auth'] != 'y' || isset($_GET['close'])) {
    session_destroy();
    header("location: ./login.php");
}
require_once "../datos/configdb.php";
require_once "./smarty.php";
require_once "../negocio/bitacora.php";
require_once "../negocio/dominio.php";

$tipo_user = $_SESSION['idtipoUser'];

/*Funciones para obtner el menu*/
$con = Conexion::getInstancia();
$sql = "SELECT m.* FROM menu m, menu_tipo t WHERE m.estado=true and m.is_submenu=false
                    and m.id=t.id_menu and t.id_tipo=$tipo_user";
$con->conectar();
$data = $con->ejecutar($sql);
$data = $data->GetArray(); $i=0;
foreach ($data as $menu) {
    $id_menu = $menu["id"];
    $sql_submenu = "SELECT m.* FROM menu m, menu_tipo t WHERE m.estado=true and m.is_submenu=true
                    and m.id=t.id_menu and t.id_tipo=$tipo_user and m.id_submenu=$id_menu";
    $sdata = $con->ejecutar($sql_submenu);
    $data[$i]['submenu'] = $sdata->GetArray(); $i++;
}

/* Obtenemos y seteamos la visita en la base de datos */
$bitacora = new Bitacora();

date_default_timezone_set("America/La_Paz");
$time = time();

$bitacora->setIdUsuario($_SESSION["idUser"]);
$bitacora->setUrl("http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
$bitacora->setIp($_SERVER["REMOTE_ADDR"]);
$bitacora->setFecha(date("d-m-Y", $time));
$bitacora->setHora(date("H:i:s", $time));
$bitacora->insertar();

$num_visitas = $bitacora->getNumeroVisita($_SESSION["idUser"]);
$smarty->assign("numero_visitas",$num_visitas[0]);

$dominios = new Dominio();
$reporte_dominios = $dominios->getAllReportes($_SESSION["idUser"]);
$smarty->assign("reporte_dominio", $reporte_dominios);

$con->cerrar();
$smarty->assign("usuario", $_SESSION['nombreUser']);
$smarty->assign("menu_array", $data);

?>