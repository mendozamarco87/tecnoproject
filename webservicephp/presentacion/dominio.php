<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 11/08/2016
 * Time: 21:40
 */
require_once "base.php";
require_once "../negocio/dominio.php";

//Instancia de Negocio Dominio
$dominio = new Dominio();


if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(!empty($_POST) && isset($_POST['action'])){
        switch($_POST['action']){
            case 'add':
                if(!empty($_POST['nombre_add'])){
                    $dominio->setnombre($_POST['nombre_add']);
                    $dominio->setIdUsuario($_SESSION["idUser"]);
                    if($dominio->insertar()){
                        echo "Insertado Correctamente";
                    }
                }
                break;
            case 'upd':
                if(!empty($_POST['id_update']) || !empty($_POST['nombre_update'])){
                    $dominio->setid($_POST['id_update']);
                    $dominio->setnombre($_POST['nombre_update']);
                    if($dominio->modificar()){
                        echo "Modificado Correctamente";
                    }
                }
                break;
            case 'del':
                if(!empty($_POST['id_delete'])){
                    $dominio->setid($_POST['id_delete']);
                    if($dominio->eliminar()){
                        echo "Eliminado Correctamente";
                    }
                }
                break;
        }
    }
}

//Obtenemos todos lo dominios.
$dominio_all = $dominio->getAllDominio($_SESSION["idUser"]);

$smarty->assign("dominio_all", $dominio_all);

$smarty->display($_SESSION['configTema'] . 'dominio.tpl');

?>