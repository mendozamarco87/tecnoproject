<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 12/08/2016
 * Time: 15:27
 */
require_once "base.php";
require_once "../negocio/usuario.php";

$n = new usuarios();
$data = $n->get($_SESSION['idUser']);
$smarty->assign("nombre", $data['nombre']);
$smarty->assign("app", $data['app']);
$smarty->assign("apm", $data['apm']);
$smarty->assign("correo", $data['correo']);
$smarty->assign("telefono", $data['telefono']);
$smarty->assign("username", $data['username']);
if (isset($_GET['edit'])) {
    $smarty->assign("edit", 'y');
}
$smarty->display($_SESSION['configTema'] . 'account.tpl');