<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 12/08/2016
 * Time: 11:41
 */
require_once "base.php";
require_once "../negocio/permisos.php";
require_once "../negocio/dominio.php";

$permiso = new Permisos();
$dominio = new Dominio();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(!empty($_POST) && isset($_POST['action'])){
        switch($_POST['action']){
            case 'add':
                if(!empty($_POST['id_dominio_add']) || !empty($_POST['ip_add'])){
                    $permiso->setid_dominio($_POST['id_dominio_add']);
                    $permiso->setip($_POST['ip_add']);
                    if($permiso->insertar()){
                        echo "Insertado Correctamente";
                    }
                }
                break;
            case 'upd':
                if(!empty($_POST['id_update']) || !empty($_POST['ip_update']) || !empty($_POST['mod_id_dominio'])){
                    $permiso->setid($_POST['id_update']);
                    $permiso->setid_dominio($_POST['mod_id_dominio']);
                    $permiso->setip($_POST['ip_update']);
                    if($permiso->modificar()){
                        echo "Modificado Correctamente";
                    }
                }
                break;
            case 'del':
                if(!empty($_POST['id_delete'])){
                    $permiso->setid($_POST['id_delete']);
                    if($permiso->eliminar()){
                        echo "Eliminado Correctamente";
                    }
                }
                break;
        }
    }
}

//Obtenemos todos los datos para guardar
$dominios_all = $dominio->getAllDominio($_SESSION["idUser"]);
$permisos_all = $permiso->getAllPermisos($_SESSION["idUser"]);

$smarty->assign("dominios_all", $dominios_all);
$smarty->assign("permisos_all", $permisos_all);

$smarty->display($_SESSION['configTema'] . 'permisos.tpl');

?>