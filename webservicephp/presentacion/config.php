<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 12/08/2016
 * Time: 20:52
 */
require_once "base.php";
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (isset($_POST['config'])){
        include "../negocio/usuario.php";
        $n = new usuarios();
        $n->setId($_SESSION['idUser']);
        $n->actualizarConfig($_POST['config']);
        $_SESSION['configTema'] = $_POST['config'];
    }
}
$smarty->display($_SESSION['configTema'] . 'config.tpl');