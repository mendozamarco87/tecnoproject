<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Tecno Web</title>
    <link href="templates/ligth_blue/white/css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="logo">
    <h4><a href="index.php">Light <strong>Blue</strong></a></h4>
</div>
<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li class="active">
            <a href="index.php"><i class="fa fa-home"></i> <span class="name">Inicio</span></a>
        </li>
        {foreach from=$menu_array item=menu}
            {if empty($menu.submenu)}
                <li class="active">
                    <a href="{$menu.accion}"><i class="fa fa-home"></i> <span class="name">{$menu.nombre}</span></a>
                </li>
            {else}
                <li class="panel">
                    <a class="accordion-toggle collapsed" data-toggle="collapse"
                       data-parent="#side-nav" href="#{$menu.id}"><i class="fa fa-link"></i> <span
                                class="name">{$menu.nombre}</span></a>
                    <ul id="{$menu.id}" class="panel-collapse collapse ">
                        {assign var="sub_menu_array" value=$menu.submenu}
                        {foreach from=$sub_menu_array item=sub_menu}
                        <li class=""><a href="{$sub_menu.accion}">{$sub_menu.nombre}</a></li>
                    </ul>
                    {/foreach}
                </li>
            {/if}
        {/foreach}
        <li class="visible-xs">
            <a href="login.php?close"><i class="fa fa-sign-out"></i> <span class="name">Sign Out</span></a>
        </li>
    </ul>
</nav>
<div class="wrap">
    <header class="page-header">
        <div class="navbar">
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="visible-phone-landscape">
                    <a href="#" id="search-toggle">
                        <i class="fa fa-search"></i>
                    </a>
                </li>
                <li class="">
                    <a href="config.php" id="settings" title="Settings">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li class="">
                    <a href="#" title="Account" id="account"
                       class="dropdown-toggle"
                       data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" class="dropdown-menu account" role="menu">
                        <li role="presentation" class="account-picture">
                            <img src="templates/ligth_blue/white/img/1.png" alt="">
                            {$usuario}
                        </li>
                        <li role="presentation">
                            <a href="account.php" class="link">
                                <i class="fa fa-user"></i>
                                Perfil
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="account.php?edit" class="link">
                                <i class="fa fa-pencil-square-o"></i>
                                Editar
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="divider"></li>
                <li class="visible-xs">
                    <a href="#"
                       class="btn-navbar"
                       data-toggle="collapse"
                       data-target=".sidebar"
                       title="">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="hidden-xs"><a href="login.php?close"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
            <form id="search-form" class="navbar-form pull-right" role="search">
                <input type="search" class="form-control search-query" placeholder="Search...">
            </form>
            {if isset($msg)}
                <div class="notifications pull-right">
                    <div class="alert pull-right">
                        <a href="#" class="close ml-xs" data-dismiss="alert">&times;</a>
                        <i class="fa fa-info-circle mr-xs"></i>{$msg}
                    </div>
                </div>
            {/if}
        </div>
    </header>
    <div class="content container">