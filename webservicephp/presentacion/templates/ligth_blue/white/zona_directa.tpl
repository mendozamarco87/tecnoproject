{include 'templates/ligth_blue/white/header.tpl'}
<h2 class="page-title">Zona Directa</h2>
<div class="row">
    <div class="col-md-4">
        <section class="widget">
            <header>
                <h4><i class="fa fa-plus"></i>&nbsp;Registrar</h4>
            </header>
            <form method="post" action="#">
                <input type="hidden" name="accion" value="reg">
                <div class="body">
                    <fieldset>
                        <div class="form-group">
                            <label for="exp">Dominio</label>
                            <select id="website" class="form-control" name="reg_id_dominio">
                                {foreach from=$dominios item=dominio}
                                    <option value="{$dominio.id}">{$dominio.nombre}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exp">Ip</label>
                            <input id="exp" class="form-control" type="text" name="reg_ip" placeholder="0.0.0.0" required>
                        </div>
                        <div class="form-group">
                            <label for="created_by">Correo responsable</label>
                            <input id="created_by" class="form-control" type="text" name="reg_cresponsable"
                                   placeholder="example.midominio.com" required>
                        </div>
                    </fieldset>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
    <div class="col-md-8">
        <section class="widget">
            {*<header>*}
            {*<div class="widget-controls">*}
            {*<a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>*}
            {*<a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-chevron-down"></i></a>*}
            {*<a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>*}
            {*</div>*}
            {*</header>*}
            <div class="body">
                {*<p>&nbsp;</p>*}
                <div class="mt table-responsive">
                    <table id="datatable-table" class="table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Dominio</th>
                            <th>Ip</th>
                            <th>Responsable</th>
                            <th class="no-sort">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        {section name=reng loop=$zonadirecta}
                            <tr>
                                <td class="fw-semi-bold">{$zonadirecta[reng].id}</td>
                                <td class="fw-semi-bold">{$zonadirecta[reng].dominio}</td>
                                <td class="fw-semi-bold">{$zonadirecta[reng].ip}</td>
                                <td class="fw-semi-bold">{$zonadirecta[reng].correo_responsable}</td>
                                <td>
                                    <button type="button" class="btn btn-warning"
                                            onclick="open_modal_modif('{$zonadirecta[reng].id}', '{$zonadirecta[reng].id_dominio}',
                                                    '{$zonadirecta[reng].ip}', '{$zonadirecta[reng].correo_responsable}')">
                                        <i class="fa fa-pencil"></i></button>
                                    <button type="button" class="btn btn-danger"  onclick="open_modal_elim('{$zonadirecta[reng].id}')">
                                        <i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        {/section}
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<!----------- MODAL UPDATE ------->
<script>
    function open_modal_modif(id, id_dominio, ip, correo_responsable) {
        $("#mod_id_dominio option:eq("+id_dominio+")").prop("selected", "selected");
        $("#mod_id_zonadirecta").val(id);
        $("#mod_ip").val(ip);
        $("#mod_cresponsable").val(correo_responsable);
        $('#modal_modif').modal('show');
    }
</script>
<div id="modal_modif" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">MODIFICAR ZONA DIRECTA</h4>
            </div>
            <form action="#" method="post" id="frm_modif" name="frm_modif">
                <div class="modal-body">
                    <input type="hidden" name="accion" value="mod">
                    <div class="form-group">
                        <input type="hidden" name="mod_id_zonadirecta" id="mod_id_zonadirecta">
                        <label for="tipo">DOMINIO: </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <select name="mod_id_dominio" class="form-control input-lg" id="mod_id_dominio">
                                {foreach from=$dominios item=dominio}
                                <option value="{$dominio.id}">{$dominio.nombre}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="nombre">IP: </LABEL>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input name="mod_ip" id="mod_ip" type="text" class="form-control input-lg"
                                   placeholder="0.0.0.0">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="ap">CORREO RESPONSABLE: </LABEL>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input name="mod_cresponsable" id="mod_cresponsable" type="text" class="form-control input-lg"
                                   placeholder="correo responsable">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-warning">Actualizar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    <!-- fin de la ventana emergente -->
</div>

<!----------- MODAL DELETE ------->
<script>
    function open_modal_elim(id) {
        $("#elim_id_zonadirecta").val(id);
        $('#modal_elim').modal('show');
    }
</script>
<div id="modal_elim" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">ELIMINAR ZONA DIRECTA</h4>
            </div>
            <form method="post" action="#">
                <input type="hidden" name="accion" value="elim">
                <input type="hidden" id="elim_id_zonadirecta" name="elim_id_zonadirecta">
                <div class="modal-body">
                    <p>Esta seguro que desea eliminar esta zona directa ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Eliminar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    <!-- fin de la ventana emergente -->
</div>

{include 'templates/ligth_blue/white/footer.tpl'}
