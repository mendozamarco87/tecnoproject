{include 'templates/ligth_blue/white/header.tpl'}
<h2 class="page-title">Gestionar Permisos</h2>
<div class="row">
    <div class="col-md-4">
        <section class="widget">
            <header>
                <h4><i class="fa fa-plus"></i> Registrar Nuevo</h4>
            </header>
            <div class="body">
                <form method="post">
                    <fieldset>
                        <div class="form-group">
                            <label for="exp">Dominio :</label>
                            <select id="website" class="form-control" name="id_dominio_add">
                                {foreach from=$dominios_all item=dominio}
                                    <option value="{$dominio.id}">{$dominio.nombre}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exp">IP :</label>
                            <input class="form-control date-picker" type="text" name="ip_add"
                                   placeholder="Ip con permisos" value="" required>
                            <input name="action" value="add" type="hidden">
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success center-block">Guardar Nuevo</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </section>
    </div>

    <div class="col-md-8">
        <section class="widget">
            <header>
                <h4>Nro. <span class="fw-semi-bold">de Resultados.</span></h4>
                <div class="widget-controls">
                    <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a data-widgster="collapse" title="Collapse" href="#"><i
                                class="glyphicon glyphicon-chevron-down"></i></a>
                    <a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </header>
            <div class="body">
                <div class="mt">
                    <table id="datatable-table" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Dominio</th>
                            <th>Ip</th>
                            <th class="no-sort">Operaciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$permisos_all item=permisos}
                            <tr>
                                <td>{$permisos.id}</td>
                                <td><span class="fw-semi-bold">{$permisos.nombre_dominio}</span></td>
                                <td><span class="fw-semi-bold">{$permisos.ip}</span></td>
                                <td>
                                    <button type="button" class="btn btn-warning"
                                            onclick="open_model_update('{$permisos.id}','{$permisos.id_dominio}','{$permisos.ip}')">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                            onclick="open_model_delete('{$permisos.id}','{$permisos.nombre_dominio}','{$permisos.ip}')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <!-- Ventanas emergentes -->
    <!-- Eliminar -->
    <div id="modalDel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h4 class="modal-title" id="myModalLabel2">ADVERTENCIA</h4>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro de eliminar este permiso ...? </p>
                        <h4 id="dominio_delete"></h4>
                        <h4 id="ip_delete"></h4>
                        <input name="id_delete" id="id_delete" type="hidden">
                        <input name="action" value="del" type="hidden">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Modificar -->
    <div id="modalUpd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
         style="display: none;">
        <div class="modal-dialog">
            <form method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                        <h4 class="modal-title" id="myModalLabel2">MODIFICAR</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exp" class="col-sm-4 control-label">Dominio :</label>
                            <div class="col-sm-7">
                                <select id="mod_id_dominio" class="form-control" name="mod_id_dominio">
                                    {foreach from=$dominios_all item=dominio}
                                        <option value="{$dominio.id}">{$dominio.nombre}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="normal-field" class="col-sm-4 control-label">Ip: </label>
                            <div class="col-sm-7">
                                <input type="hidden" name="id_update" id="id_update">
                                <input type="text" name="ip_update" id="ip_update" class="form-control"
                                       placeholder="Nombre del Dominio">
                            </div>
                            <input name="action" value="upd" type="hidden">
                        </div>
                    </div>
                    <br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-warning">Modificar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
        </div>
        <!-- /.modal-dialog -->
    </div>


    <!-- Funciones SCRIPT -->
    <script>
        function open_model_update(id, id_dominio, ip) {
            $("#mod_id_dominio option:eq("+id_dominio+")").prop("selected", "selected");
            $("#id_update").val(id);
            $("#ip_update").val(ip);
            $("#modalUpd").modal("show");
        }

        function open_model_delete(id,nombre,ip){
            $("#id_delete").val(id);
            $("#dominio_delete").text(nombre);
            $("#ip_delete").text(ip);
            $("#modalDel").modal("show");
        }
    </script>

</div>
{include 'templates/ligth_blue/white/footer.tpl'}
