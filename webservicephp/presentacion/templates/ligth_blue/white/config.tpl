{include 'templates/ligth_blue/white/header.tpl'}
<h2 class="page-title">Mi <span class="fw-semi-bold">Config</span> <small>Temas disponibles</small></h2>
<div class="row">
    <div class="col-md-10">
        <ul class="row thumbnails">
            <li class="col-sm-4">
                <div class="thumbnail">
                    <img src="templates/ligth_blue/white/img/light-blue.png" alt="" style="height: 200px">
                    <h4>&nbsp;Thumbnail label</h4>
                    <p>&nbsp;&nbsp;Template number 3</p>
                    <form method="post" action="#">
                    <div class="form-actions">
                        <input type="hidden" name="config" value="ligth_blue/dark/">
                        <button class="btn btn-success pull-right"><i class="fa fa-check"></i> Aplicar</button>
                    </div>
                    </form>
                </div>
            </li>
            <li class="col-sm-4">
                <div class="thumbnail">
                    <img src="templates/ligth_blue/white/img/light-blue-white.png" alt="" style="height: 200px">
                    <h4>&nbsp;Thumbnail label</h4>
                    <p>&nbsp;&nbsp;Template number 3</p>
                    <form method="post" action="#">
                        <div class="form-actions">
                            <input type="hidden" name="config" value="ligth_blue/white/">
                            <button class="btn btn-success pull-right"><i class="fa fa-check"></i> Aplicar</button>
                        </div>
                    </form>
                </div>
            </li>
            <li class="col-sm-4">
                <div class="thumbnail">
                    <img src="templates/ligth_blue/white/img/material.png" alt="" style="height: 200px">
                    <h4>&nbsp;Thumbnail label</h4>
                    <p>&nbsp;&nbsp;Template number 3</p>
                    <form method="post" action="#">
                        <div class="form-actions">
                            <input type="hidden" name="config" value="material/">
                            <button class="btn btn-success pull-right"><i class="fa fa-check"></i> Aplicar</button>
                        </div>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
{include 'templates/ligth_blue/white/footer.tpl'}