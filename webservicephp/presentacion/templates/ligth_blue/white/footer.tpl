</div>
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin"></i>
</div>
</div>
<!-- common libraries. required for every page-->
<script src="templates/ligth_blue/white/lib/jquery/dist/jquery.min.js"></script>
<script src="templates/ligth_blue/white/lib/jquery-pjax/jquery.pjax.js"></script>
<script src="templates/ligth_blue/white/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="templates/ligth_blue/white/lib/widgster/widgster.js"></script>
<script src="templates/ligth_blue/white/lib/underscore/underscore.js"></script>

<!-- common application js -->
<script src="templates/ligth_blue/white/js/app.js"></script>
<script src="templates/ligth_blue/white/js/settings.js"></script>

<!-- Libs Tables -->
<script src="templates/ligth_blue/white/lib/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="templates/ligth_blue/white/lib/underscore/underscore.js"></script>
<script src="templates/ligth_blue/white/lib/backbone/backbone.js"></script>
<script src="templates/ligth_blue/white/lib/backbone.paginator/lib/backbone.paginator.min.js"></script>
<script src="templates/ligth_blue/white/lib/backgrid/lib/backgrid.min.js"></script>
<script src="templates/ligth_blue/white/lib/backgrid-paginator/backgrid-paginator.js"></script>
<script src="templates/ligth_blue/white/lib/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="templates/ligth_blue/white/js/tables-dynamic.js"></script>
<script>
    $.pjax.disable();
</script>


</body>
</html>