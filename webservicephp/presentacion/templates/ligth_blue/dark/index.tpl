{include 'templates/ligth_blue/dark/header.tpl'}
<h2 class="page-title">Inicio<small></small></h2>
<div class="row">
    <div class="col-md-12">
        <div class="col-sm-6">
            <section class="widget widget-overview">
                <header>
                    <h4>
                        <i class="fa fa-lightbulb"></i>
                        Estadisticas de dominio
                    </h4>
                </header>
                <div class="body">
                    <ul class="overall-stats">
                        <li>
                            <div class="icon pull-left">
                                <i class="fa fa-cloud"></i>
                            </div>
                            <span class="key">Dominios .COM</span>
                            <div class="value pull-right">
                                <span class="badge badge-danger">{$reporte_dominio[0].total} %</span>
                            </div>
                        </li>
                        <li>
                            <div class="icon pull-left">
                                <i class="fa fa-cloud"></i>
                            </div>
                            <span class="key">Dominios .BO</span>
                            <div class="value pull-right">
                                <span class="badge badge-warning">{$reporte_dominio[1].total} %</span>
                            </div>
                        </li>
                        <li>
                            <div class="icon pull-left">
                                <i class="fa fa-cloud"></i>
                            </div>
                            <span class="key">Dominios .EDU</span>
                            <div class="value pull-right">
                                <span class="badge badge-info">{$reporte_dominio[2].total} %</span>
                            </div>
                        </li>
                        <li>
                            <div class="icon pull-left">
                                <i class="fa fa-cloud"></i>
                            </div>
                            <span class="key">Dominios .ORG</span>
                            <div class="value pull-right">
                                <span class="badge badge-default">{$reporte_dominio[3].total} %</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div>
{include 'templates/ligth_blue/dark/footer.tpl'}