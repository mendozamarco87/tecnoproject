<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <title>Tecno Web</title>

    <link href="templates/ligth_blue/dark/css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="single-widget-container">
    <section class="widget login-widget">
        <header class="text-align-center">
            <h4>Login to your account</h4>
        </header>
        <div class="body">
            <form class="no-margin"
                  action="#" method="post">
                <fieldset>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                            <input name="user" id="user" type="text" class="form-control input-lg input-transparent"
                                   placeholder="Your Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>

                        <div class="input-group input-group-lg">
                                    <span class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </span>
                            <input name="password" id="password" type="password" class="form-control input-lg input-transparent"
                                   placeholder="Your Password">
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-block btn-lg btn-danger">
                        <span class="small-circle"><i class="fa fa-caret-right"></i></span>
                        <small>Sign In</small>
                    </button>
                    <a class="forgot" href="#">Forgot Username or Password?</a>
                </div>
            </form>
        </div>
        <footer>
            <div class="facebook-login">
                <a href="index.html"><span><i class="fa fa-facebook-square fa-lg"></i> LogIn with Facebook</span></a>
            </div>
        </footer>
    </section>
</div>
<script src="templates/ligth_blue/dark/lib/jquery/dist/jquery.min.js"></script>
<script src="templates/ligth_blue/dark/lib/jquery-pjax/jquery.pjax.js"></script>
<script src="templates/ligth_blue/dark/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="templates/ligth_blue/dark/lib/widgster/widgster.js"></script>
<script src="templates/ligth_blue/dark/lib/underscore/underscore.js"></script>
<script src="templates/ligth_blue/dark/js/app.js"></script>
<script src="templates/ligth_blue/dark/js/settings.js"></script>
</body>
</html>