<div class="row">
    <div class="col-md-12">
        <div class="col-md-3 pull-right">
            <div class="box">
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <div class="description">
                    <strong>{$numero_visitas.total_visitas}</strong> visitas
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin"></i>
</div>
</div>
<!-- common libraries. required for every page-->
<script src="templates/ligth_blue/dark/lib/jquery/dist/jquery.min.js"></script>
<script src="templates/ligth_blue/dark/lib/jquery-pjax/jquery.pjax.js"></script>
<script src="templates/ligth_blue/dark/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="templates/ligth_blue/dark/lib/widgster/widgster.js"></script>
<script src="templates/ligth_blue/dark/lib/underscore/underscore.js"></script>

<!-- common application js -->
<script src="templates/ligth_blue/dark/js/app.js"></script>
<script src="templates/ligth_blue/dark/js/settings.js"></script>

<script>
    $.pjax.disable()
</script>

<!-- Libs Tables -->
<script src="templates/ligth_blue/dark/lib/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="templates/ligth_blue/dark/lib/underscore/underscore.js"></script>
<script src="templates/ligth_blue/dark/lib/backbone/backbone.js"></script>
<script src="templates/ligth_blue/dark/lib/backbone.paginator/lib/backbone.paginator.min.js"></script>
<script src="templates/ligth_blue/dark/lib/backgrid/lib/backgrid.min.js"></script>
<script src="templates/ligth_blue/dark/lib/backgrid-paginator/backgrid-paginator.js"></script>
<script src="templates/ligth_blue/dark/lib/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="templates/ligth_blue/dark/js/tables-dynamic.js"></script>

</body>
</html>