{include 'templates/ligth_blue/dark/header.tpl'}
<h2 class="page-title">Bitacora</h2>
<div class="row">
    <div class="col-md-12">
        <section class="widget">
            <header>
                <h4>Nro. <span class="fw-semi-bold">de Resultados.</span></h4>
                <div class="widget-controls">
                    <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a data-widgster="collapse" title="Collapse" href="#"><i
                                class="glyphicon glyphicon-chevron-down"></i></a>
                    <a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </header>
            <div class="body">
                <div class="mt">
                    <table id="datatable-table" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Id Usuario</th>
                            <th>Url</th>
                            <th>Ip</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$bitacora_all item=bitacora}
                            <tr>
                                <td>{$bitacora.id_usuario}</td>
                                <td>{$bitacora.url}</td>
                                <td>{$bitacora.ip}</td>
                                <td>{$bitacora.fecha}</td>
                                <td>{$bitacora.hora}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
{include 'templates/ligth_blue/dark/footer.tpl'}