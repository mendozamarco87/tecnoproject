{include 'templates/ligth_blue/dark/header.tpl'}
<h2 class="page-title">Mi Perfil</h2>
<div class="row">
    <div class="col-md-7">
        <section class="widget">
            <header>
                <h4><i class="fa fa-user"></i> Account Profile</h4>
                <div class="actions">
                    <a href="?edit">
                        <button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></button>
                    </a>
                </div>
            </header>
            <div class="body">
                <form id="user-form" class="form-horizontal form-label-left"
                      novalidate="novalidate"
                      method="post"
                      data-parsley-priority-enabled="false"
                      data-parsley-excluded="input[name=gender]">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="text-align-center">
                                <img class="img-circle" src="templates/ligth_blue/dark/img/1.png" alt="64x64"
                                     style="height: 112px;">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h3 class="mt-sm mb-xs">{$nombre}&nbsp;{$app}&nbsp;{$apm}</h3>
                            <address>
                                <abbr title="Work email">e-mail:</abbr> <a href="mailto:#">{$correo}</a><br>
                                <abbr title="Work Phone">phone:</abbr> {$telefono}
                            </address>
                        </div>
                    </div>
                    <fieldset>
                        <legend class="section">Personal Info</legend>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="prefix">Username</label>
                            <div class="col-sm-4"><input id="prefix" type="text" class="form-control input-transparent"
                                        {if !isset($edit)} disabled="disabled" {/if} value="{$username}"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="first-name">Nombres</label>
                            <div class="col-sm-8"><input id="first-name" type="text"
                                                         class="form-control input-transparent"
                                        {if !isset($edit)} disabled="disabled" {/if} value="{$nombre}"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="last-name">Apellido Paterno</label>
                            <div class="col-sm-8"><input id="last-name" type="text"
                                                         class="form-control input-transparent"
                                        {if !isset($edit)} disabled="disabled" {/if} value="{$app}"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="last-name">Apellido Materno</label>
                            <div class="col-sm-8"><input id="last-name" type="text"
                                                         class="form-control input-transparent"
                                        {if !isset($edit)} disabled="disabled" {/if} value="{$apm}"></div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend class="section">Contact Info</legend>
                        <div class="form-group">
                            <label class="control-label col-sm-4" for="email">Email</label>
                            <div class="col-sm-8">
                                <input id="email" type="email"
                                       class="form-control input-transparent" {if !isset($edit)} disabled="disabled" {/if}
                                       value="{$correo}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label col-sm-4">Telefono</label>
                            <div class="col-sm-6">
                                <input id="phone" class="form-control input-transparent"
                                       type="text" {if !isset($edit)} disabled="disabled" {/if} value="{$telefono}">
                            </div>
                        </div>
                    </fieldset>
                    {if isset($edit)}
                        <div class="form-actions">
                            <button type="submit" class="btn btn-warning center-block">Actualizar</button>
                        </div>
                    {/if}
                </form>
            </div>
        </section>
    </div>
    {if isset($edit)}
        <div class="col-md-5">
            <section class="widget">
                <header>
                    <h4><i class="fa fa-cogs"></i> Cambiar Password</h4>
                </header>
                <div class="body">
                    <form method="post">
                        <fieldset>
                            <div class="form-group">
                                <label for="exp">Actual</label>
                                <input id="exp" class="form-control date-picker" type="password" name="exp" value="">
                            </div>
                            <div class="form-group">
                                <label for="exp">Nuevo</label>
                                <input id="exp" class="form-control date-picker" type="password" name="exp" value="">
                            </div>
                            <div class="form-group">
                                <label for="exp">Confirmar</label>
                                <input id="exp" class="form-control date-picker" type="password" name="exp" value="">
                            </div>
                        </fieldset>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-warning center-block">Actualizar</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    {/if}
</div>
{include 'templates/ligth_blue/dark/footer.tpl'}