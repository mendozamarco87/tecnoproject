{include 'templates/ligth_blue/dark/header.tpl'}
<h2 class="page-title">USUARIO </span></h2>
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#Modalregistrar">Registrar</button>
<section class="widget">
    <header>
        <div class="widget-controls">
            <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
        </div>
    </header>
    <div class="body">
        <p>
        </p>
        <div class="mt table-responsive">
            <table id="datatable-table" class="table table-striped table-hover ">
                <thead>
                <tr>
                    <th>codigo</th>
                    <th class="">nombre</th>
                    <th class="">ap</th>
                    <th class="fw-semi-bold">am</th>
                    <th class="fw-semi-bold">telefono</th>
                    <th class="fw-semi-bold">correo</th>
                    <th class="no-sort">opciones</th>
                </tr>
                </thead>
                <tbody>
                {section name=reng loop=$user}
                    <tr>
                        <td>{$user[reng].id}</td>
                        <td><span class="fw-semi-bold">{$user[reng].nombre}</span></td>
                        <td class="fw-semi-bold">{$user[reng].app}</a></td>
                        <td class="fw-semi-bold">{$user[reng].apm}</td>
                        <td class="fw-semi-bold">{$user[reng].telefono}</td>
                        <td class="fw-semi-bold">{$user[reng].correo}</td>
                        <td>
                            <button type="button" class="btn btn-warning"
                                    onclick="fn_modi('{$user[reng].id}','{$user[reng].nombre}',
                                            '{$user[reng].app}','{$user[reng].apm}','{$user[reng].telefono}',
                                            '{$user[reng].correo}',{$user[reng].tipo});">
                                <i class="fa fa-pencil"></i></button>
                            <button type="button" class="btn btn-danger"  onclick="fn_eliminar({$user[reng].id});">
                                <i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                {/section}
                </tbody>
            </table>
        </div>
    </div>
</section>
<script language="javascript" type="text/javascript">
    function fn_modi(idusuario, nombreuser, apelldoP, apelldoM, telefonou, correou, tipou) {
        $('#tipom').val(tipou);
        $('#idm').val(idusuario);
        $('#nombrem').val(nombreuser);
        $('#appm').val(apelldoP);
        $('#apmm').val(apelldoM);
        $('#telefonom').val(telefonou);
        $('#correom').val(correou);
        $('#ModalActualizar').modal('show');
    }
    ;
</script>
<!--- ------------------ventana emergente registrar -->
<div id="Modalregistrar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">REGISTRAR USUARIO</h4>
            </div>
            <div class="modal-body">
                <FORM action="javascript: fn_agregar();" method="post" id="frm_agregar_usuario"
                      name="frm_agregar_usuario">
                    <div class="form-group">
                        <label for="tipo">tipoUser</label>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <select name="tipo" class="form-control input-lg" id="tipo">
                                <option value="1">Administrador</option>
                                <option value="2">Invitado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="nombre">Nombre: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="nombre" id="nombre" type="text" class="form-control input-lg"
                                   placeholder="nombre" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="ap">Apellido Paterno: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="app" id="app" type="text" class="form-control input-lg"
                                   placeholder="Apellido Paterno" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="Am">Apellido Materno: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="apm" id="apm" type="text" class="form-control input-lg"
                                   placeholder="Apellido materno" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="telefono">telefono: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="telefono" id="telefono" type="number" class="form-control input-lg"
                                   placeholder="telefono" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="correo">correo: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="correo" id="correo" type="email" class="form-control input-lg"
                                   placeholder="correo" required="">
                        </div>
                    </div>
                    <input type="submit" name="modificar" id="btnModificar" class="btn btn-success">
                    <input type="reset" name="nuevo" id="btnNuevo" class="btn btn-transparent">
                </FORM>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <!--   <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- fin de la ventana emergente registrar Usuario -->
<!--- ------------------ventana emergente actualizar -->
<div id="ModalActualizar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel2">MODIFICAR DATOS</h4>
            </div>
            <div class="modal-body">
                <FORM action="javascript: fn_modificar();" method="post" id="frm_modificar_usuario"
                      name="frm_modificar_usuario">
                    <input name="id" id="idm" type="text" hidden="">
                    <div class="form-group">
                        <label for="tipo">tipoUser</label>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <select name="tipo" class="form-control input-lg" id="tipom">
                                <option value="1">Administrador</option>
                                <option value="2">Invitado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="nombre">Nombre: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="nombre" id="nombrem" type="text" class="form-control input-lg"
                                   placeholder="nombre" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="ap">Apellido Paterno: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="app" id="appm" type="text" class="form-control input-lg"
                                   placeholder="Apellido Paterno" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="Am">Apellido Materno: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="apm" id="apmm" type="text" class="form-control input-lg"
                                   placeholder="Apellido materno" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="telefono">telefono: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="telefono" id="telefonom" type="number" class="form-control input-lg"
                                   placeholder="telefono" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <LABEL for="correo">correo: </LABEL>
                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </span>
                            <input name="correo" id="correom" type="email" class="form-control input-lg"
                                   placeholder="correo" required="">
                        </div>
                    </div>
                    <input type="submit" name="actualizar" id="btnActualizar" class="btn btn-success">
                </FORM>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <!--   <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- fin de la ventana emergente actualizar usuario -->
<div class="loader-wrap hiding hide">
    <i class="fa fa-circle-o-notch fa-spin"></i>
</div>
</div>
</div>

<script language="javascript" type="text/javascript" src="../js/usuario.js"></script>
<!-- common libraries. required for every page-->
<script src="templates/ligth_blue/dark/lib/jquery/dist/jquery.min.js"></script>
<script src="templates/ligth_blue/dark/lib/jquery-pjax/jquery.pjax.js"></script>
<script src="templates/ligth_blue/dark/lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="templates/ligth_blue/dark/lib/widgster/widgster.js"></script>
<script src="templates/ligth_blue/dark/lib/underscore/underscore.js"></script>

<!-- common application js -->
<script src="templates/ligth_blue/dark/js/app.js"></script>
<script src="templates/ligth_blue/dark/js/settings.js"></script>

<!-- common templates -->
<script type="text/template" id="settings-template">
 
        </script>

<script type="text/template" id="sidebar-settings-template">

        </script>
<!-- page specific scripts -->
<!-- page specific libs -->
<script src="templates/ligth_blue/dark/lib/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="templates/ligth_blue/dark/lib/underscore/underscore.js"></script>
<script src="templates/ligth_blue/dark/lib/backbone/backbone.js"></script>
<script src="templates/ligth_blue/dark/lib/backbone.paginator/lib/backbone.paginator.min.js"></script>
<script src="templates/ligth_blue/dark/lib/backgrid/lib/backgrid.min.js"></script>
<script src="templates/ligth_blue/dark/lib/backgrid-paginator/backgrid-paginator.js"></script>
<script src="templates/ligth_blue/dark/lib/datatables/media/js/jquery.dataTables.min.js"></script>
<!-- page application js -->
<script src="templates/ligth_blue/dark/js/tables-dynamic.js"></script>
</body>
</html>