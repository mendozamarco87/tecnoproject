<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 11/08/2016
 * Time: 21:37
 */
require_once "base.php";

require_once '../negocio/zona_directa.php';
require_once '../negocio/dominio.php';
$n = new zona_directa();
$d = new Dominio();
//print_r($data);
if (!empty($_POST) && isset($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 'reg':
            registrar();
            break;
        case 'mod':
            modificar();
            break;
        case 'elim':
            eliminar();
            break;
        case 'backup':
            do_backup();
            break;
    }
}
$data = $n->getMisZonaDirectas($_SESSION['idUser']);
$dominios = $d->getAllDominio($_SESSION['idUser']);
$smarty->assign("zonadirecta", $data);
$smarty->assign("dominios", $dominios);
$smarty->display($_SESSION['configTema'] . 'zona_directa.tpl');

function registrar(){
    global $n, $smarty;
    $n->setid_dominio($_POST['reg_id_dominio']);
    $n->setip($_POST['reg_ip']);
    $n->setcorreo_responsable($_POST['reg_cresponsable']);
    if ($n->insertar()) {
        $smarty->assign("msg", "Se registro correctamente..");
    } else {
        $smarty->assign("msg", "Ocurrio algun error, intentalo nuevamente..");
    }
}

function modificar(){
    global $n, $smarty;
    $n->setid($_POST['mod_id_zonadirecta']);
    $n->setid_dominio($_POST['mod_id_dominio']);
    $n->setip($_POST['mod_ip']);
    $n->setcorreo_responsable($_POST['mod_cresponsable']);
    if ($n->modificar()) {
        $smarty->assign("msg", "Se actualizo correctamente..");
    } else {
        $smarty->assign("msg", "Ocurrio algun error, intentalo nuevamente..");
    }
}

function eliminar(){
    global $n, $smarty;
    $n->setid($_POST['elim_id_zonadirecta']);
    if ($n->eliminar()) {
        $smarty->assign("msg", "Se elimino correctamente..");
    } else {
        $smarty->assign("msg", "Ocurrio algun error, intentalo nuevamente..");
    }
}

function do_backup(){
    if (!isset($_POST['id_dominio']) || !isset($_POST['id_zonadirecta']))
        return;
    global $n, $smarty;
    require_once "../negocio/backup.php";
    $n = new backup($_POST['id_dominio']);
    if ($arch = $n->generarZonaDirecta($_POST['id_zonadirecta'])){
        header('Location: ' . $arch);
    };
}