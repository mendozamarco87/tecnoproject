<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 13/08/2016
 * Time: 3:12
 */

require_once "base.php";
require_once "../negocio/bitacora.php";

//Instancia de Bitacora.
$bitacora = new Bitacora();


//Obtenemos todos lo dominios.
$bitacora_all = $bitacora->getAll();

$smarty->assign("bitacora_all", $bitacora_all);

$smarty->display($_SESSION['configTema'] . 'bitacora.tpl');

?>