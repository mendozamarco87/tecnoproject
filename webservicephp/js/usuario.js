function fn_agregar() {
    $("#btnAgregar").hide();
    var str = $("#frm_agregar_usuario").serialize();
    $.ajax({
        url: '../negocio/abmUsuario.php',
        data: str + '&proc=agregar',
        type: 'get',
        success: function(data) {
            if ($.trim(data) != "correcto") {
                alert(data);
            } else {
                alert("se registro correctamente");
            }
        }
    });
};
function fn_modificar() {
    $("#btnModificar").hide();
    var str = $("#frm_modificar_usuario").serialize();
    $.ajax({
        url: '../negocio/abmUsuario.php',
        data: str + '&proc=modif',
        type: 'get',
        success: function(data) {
            if ($.trim(data) != "correcto") {
                alert(data);
            } else {
                alert("se actualizo correctamente");
            }
        }
    });
};
function fn_eliminar(id_user){
	var respuesta = confirm("Desea eliminar este Usuarios?");
	if (respuesta){
            $.ajax({
                url: '../negocio/abmUsuario.php',
                data: 'proc=eliminar&id=' + id_user,
                type: 'post',
                success: function(data){
                    if($.trim(data)!="correcto"){
                        alert(data);
                    }else{
                        alert("Se elimino correctamente");
                    }
                    $.unblockUI();
                }
            });
	}
};
function fn_mostrarDatosModificar(iduser,nombre,app,apm,telefono,correo) {
    $('#idm').val(iduser);
    $('#nombrem').val(nombre);
    $('#appm').val(app);
    $('#apmm').val(apm);
    $('#telefonom').val(telefono);
    $('#correom').val(correo);
    $.blockUI({
        message: $('#ModalActualizar'),
        css: {
            top: '5%',
            left: '20%',
            width: '65%',
            bottom: '5%'
        }
    });
};
