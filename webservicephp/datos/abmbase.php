<?php
/**
 * Created by PhpStorm.
 * User: mendo
 * Date: 05/08/2016
 * Time: 18:03
 */
require_once "configdb.php";


class ABMbase
{
    private static $conexion;
    
    function get($id){
        $sql = "SELECT * FROM " . $this->table_name() . " WHERE id=" . $id;
        if ($data = $this->execute($sql)){
            return $data->GetRowAssoc();
        }else{
            print_r($data);
        }
    }

    function getAll(){
        $sql = "SELECT * FROM " . $this->table_name() . " WHERE estado = True;";
        if ($data = $this->execute($sql)){
            $c = $data->RecordCount();
            $arreglo = [];
            for($i = 0; $i < $c; $i++) {
                $arreglo[$i] = $data->GetRowAssoc();
                $data->MoveNext();
            }
            return $arreglo;
        }else{
            print_r($data);
        }
    }

    protected function insert($arrayDatos){
        $arr = $arrayDatos;
        $sql = "INSERT INTO " . $this->table_name() . "(";
        $values = "VALUES (";
        $c = count($arr); $i = 1;
        foreach ($arr as $item => $value){
            $sql .= $i == $c ? $item : $item . ", ";
            $values .= $i == $c ? "'$value'" : "'$value'" . ", ";
            $i++;
        }
        $sql .= ") " . $values . ");";
        self::$conexion = Conexion::getInstancia();
        try{
            self::$conexion->conectar();
            $rs = self::$conexion->ejecutar($sql);
            $d = Null;
            if ($rs) {
                $d = self::$conexion->getLastID();
            }
            self::$conexion->cerrar();
            return $d;
        } catch (Exception $e) {
            print_r($e->getTraceAsString());
        }
    }

    protected function modify($arrayDatos){
        $arr = $arrayDatos;
        $sql = "UPDATE " . $this->table_name() . " SET ";
        $c = count($arr); $i = 1;
        foreach ($arr as $item => $value){
            if ( $item != "WHERE") {
                $sql .= $item . "=" . "'$value'";
                $sql .= $i == $c-1 ? "" : ", ";
            } else {
                $sql .= " WHERE " . $value;
            }
            $i++;
        }
        if ($data = $this->execute($sql)){
            return true;
        }else{
            print_r($data);
        }
    }

    protected function delete($where){
        $sql = "DELETE FROM " . $this->table_name();
        $sql .= " WHERE " . $where;
        if ($data = $this->execute($sql)){
            return true;
        } else {
            print_r($data);
        }
    }

    protected function execute($sql){
        self::$conexion = Conexion::getInstancia();
        try{
            self::$conexion->conectar();
            $rs = self::$conexion->ejecutar($sql);
            self::$conexion->cerrar();
            return $rs;
        } catch (Exception $e) {
            print_r($e->getTraceAsString());
        }
    }

    public function table_name(){
        return "";
    }
}