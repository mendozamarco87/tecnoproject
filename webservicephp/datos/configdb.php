<?php

include('../utils/adodb5/adodb.inc.php');

define("DRIVER", "postgres");
define("SERVER", "localhost");
define("USER", "postgres");
define("PASS", "joeljoel");
define("DATABASE", "tecno");


class Conexion{
    private static $instacia;
    private static $adodb;

    final private function __construct(){
        try{
            self::$adodb = ADONewConnection(DRIVER);
            //self::$adodb->debug = true;
            return self::$adodb;
        } catch (Exception $e){
            echo $e;
        }
    }

    public static function getInstancia()
    {
        if (self::$instacia == null){
            self::$instacia = new self();
        }
        return self::$instacia;
    }

    public function conectar(){
        self::$adodb->Connect(SERVER, USER, PASS, DATABASE);
    }

    public function ejecutar($sql){
        return self::$adodb->Execute($sql);
    }

    public function cerrar(){
        self::$adodb->Close();
    }

    public function getLastID(){
        return self::$adodb->Insert_ID();
    }

}