CREATE TABLE menu_tipo (
	id_menu integer,
	id_tipo integer
)
;

CREATE TABLE tipo_usuario (
	id serial NOT NULL,
	nombre varchar(50),
	descripcion varchar(250),
	estado boolean
)
;

CREATE TABLE usuario (
	id serial NOT NULL,
	username varchar(50),
	pass varchar(100),
	nombre varchar(100),
	app varchar(50),
	apm varchar(50),
	telefono varchar(20),
	correo varchar(100),
	tipo integer,
	config_tema varchar(200) DEFAULT 'ligth_blue/dark/',
	estado boolean
)
;

CREATE TABLE Menu (
	id serial NOT NULL,
	id_submenu integer,
	nombre varchar(100),
	accion varchar(200),
	is_submenu boolean,
	estado boolean
)
;

CREATE TABLE permisos (
	id serial NOT NULL,
	id_dominio integer NOT NULL,
	permiso varchar(100) NOT NULL,
	estado boolean NOT NULL
)
;

CREATE TABLE zona_inversa (
	id serial NOT NULL,
	id_dominio integer NOT NULL,
	ip varchar(50) NOT NULL,
	correo_responsable varchar(200),
	estado boolean NOT NULL
)
;

CREATE TABLE zona_directa (
	id serial NOT NULL,
	id_dominio integer NOT NULL,
	ip varchar(50) NOT NULL,
	correo_responsable varchar(200),
	estado boolean NOT NULL
)
;

CREATE TABLE dominio (
	id serial NOT NULL,
	nombre text NOT NULL,
	id_usuario integer,
	estado boolean DEFAULT true NOT NULL
)
;

CREATE TABLE bitacora (
	id serial NOT NULL,
	id_usuario integer NOT NULL,
	url varchar(100) NOT NULL,
	ip varchar(50) NOT NULL,
	fecha varchar(50) NOT NULL,
	hora varchar(50) NOT NULL,
	estado boolean DEFAULT true NOT NULL
)
;


ALTER TABLE tipo_usuario ADD CONSTRAINT PK_tipo_usuario
	PRIMARY KEY (id)
;


ALTER TABLE usuario ADD CONSTRAINT PK_usuario
	PRIMARY KEY (id)
;


ALTER TABLE Menu ADD CONSTRAINT PK_Menu
	PRIMARY KEY (id)
;


ALTER TABLE permisos ADD CONSTRAINT PK_permisos
	PRIMARY KEY (id)
;


ALTER TABLE zona_inversa ADD CONSTRAINT PK_zona_inversa
	PRIMARY KEY (id)
;


ALTER TABLE zona_directa ADD CONSTRAINT PK_zona_directa
	PRIMARY KEY (id)
;


ALTER TABLE dominio ADD CONSTRAINT PK_dominio
	PRIMARY KEY (id)
;

ALTER TABLE bitacora ADD CONSTRAINT PK_bitacora
	PRIMARY KEY (id)
;



ALTER TABLE menu_tipo ADD CONSTRAINT FK_menu_tipo_Menu
	FOREIGN KEY (id_menu) REFERENCES Menu (id)
;

ALTER TABLE menu_tipo ADD CONSTRAINT FK_menu_tipo_tipo_usuario
	FOREIGN KEY (id_tipo) REFERENCES tipo_usuario (id)
;

ALTER TABLE usuario ADD CONSTRAINT FK_usuario_tipo_usuario
	FOREIGN KEY (tipo) REFERENCES tipo_usuario (id)
;

ALTER TABLE permisos ADD CONSTRAINT FK_permisos_dominio
	FOREIGN KEY (id_dominio) REFERENCES dominio (id)
;

ALTER TABLE zona_inversa ADD CONSTRAINT FK_zona_inversa_dominio
	FOREIGN KEY (id_dominio) REFERENCES dominio (id)
;

ALTER TABLE zona_directa ADD CONSTRAINT FK_zona_directa_dominio
	FOREIGN KEY (id_dominio) REFERENCES dominio (id)
;

ALTER TABLE dominio ADD CONSTRAINT FK_dominio_usuario
	FOREIGN KEY (id_usuario) REFERENCES usuario (id)
;
