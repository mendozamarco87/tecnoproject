package com.mendoza.sockets;

import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by mendoza on 05/07/2016.
 */
public class SockectServer {
    static final int PUERTO=5000;

    public SockectServer() {
        try{
            ServerSocket skServidor = new ServerSocket( PUERTO );
            System.out.println("S : Escucho el puerto " + PUERTO );
            for ( int numCli = 0; numCli < 3; numCli++ ) {
                Socket skCliente = skServidor.accept(); //Crea objeto
                System.out.println("S : Sirvo al cliente " + numCli);
                DataOutputStream salida = new DataOutputStream(skCliente.getOutputStream());

                salida.writeBytes( "Hola cliente " + numCli );
                skCliente.close();
            }
            System.out.println("S : Demasiados clientes por hoy");
        } catch( Exception e ){
            System.out.println( e.getMessage() );
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        SockectServer s = new SockectServer();
    }
}
