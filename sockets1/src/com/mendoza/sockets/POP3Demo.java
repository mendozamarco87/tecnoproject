package com.mendoza.sockets;

/**
 * Created by mendoza on 06/07/2016.
 */
import java.io.*;
import java.net.*;

class POP3Demo
{
    public static void main (String [] args)
    {
        String POP3Server = "virtual.ficct.uagrm.edu.bo";
        int POP3Port = 110;

        Socket client = null;
        
        try
        {
            // Attempt to create a client socket connected to the POP3
            // server program.

            client = new Socket (POP3Server, POP3Port);

            // Create a buffered reader for line-oriented reading from the
            // standard input device.

            BufferedReader stdin;
            stdin = new BufferedReader (new InputStreamReader (System.in));

            // Create a buffered reader for line-oriented reading from the
            // socket.

            InputStream is = client.getInputStream ();
            BufferedReader sockin;
            sockin = new BufferedReader (new InputStreamReader (is));

            // Create a print writer for line-oriented writing to the
            // socket.

            OutputStream os = client.getOutputStream ();
            PrintWriter sockout;
            sockout = new PrintWriter (os, true); // true for auto-flush

            // Display POP3 greeting from POP3 server program.

            System.out.println ("S:" + sockin.readLine ());

            while (true)
            {
                // Display a client prompt.

                System.out.print ("C:");

                // Read a command string from the standard input device.

                String cmd = stdin.readLine ();

                // Write the command string to the POP3 server program.

                sockout.println (cmd);

                // Read a reply string from the POP3 server program.

                String reply = sockin.readLine ();

                // Display the first line of this reply string.

                System.out.println ("S:" + reply);

                // If the RETR command was entered and it succeeded, keep
                // reading all lines until a line is read that begins with
                // a . character. These lines constitute an email message.

                if (cmd.toLowerCase ().startsWith ("retr") &&
                        reply.charAt (0) == '+') {
                    do {
                        reply = sockin.readLine();
                        System.out.println("S:" + reply);
                        if (reply != null && reply.length() > 0)
                            if (reply.charAt(0) == '.')
                                break;
                    }
                    while (true);
                }
                // If the QUIT command was entered, quit.

                if (cmd.toLowerCase ().startsWith ("quit"))
                    break;
            }
        }
        catch (IOException e)
        {
            System.out.println (e.toString ());
        }
        finally
        {
            try
            {
                // Attempt to close the client socket.

                if (client != null)
                    client.close ();
            }
            catch (IOException e)
            {
            }
        }
    }
}
