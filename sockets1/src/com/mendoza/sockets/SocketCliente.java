package com.mendoza.sockets;

import java.io.*;
import java.net.Socket;

/**
 * Created by mendoza on 05/07/2016.
 */
public class SocketCliente {
    static final String HOST = "localhost";
    static final int PUERTO=1125;

    public SocketCliente() {
        try{
            Socket skCliente = new Socket( HOST , PUERTO );
            BufferedReader entrada = new BufferedReader(new InputStreamReader(skCliente.getInputStream()));
            System.out.println("C : Conectado a <" + HOST + ">" );
            System.out.println("S: " + entrada.readLine() );
            skCliente.close();
            System.out.println("C : Desconectado del <" + HOST + ">" );
        } catch( Exception e ){
            System.out.println("C : " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        try {
            SocketCliente c = new SocketCliente();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
