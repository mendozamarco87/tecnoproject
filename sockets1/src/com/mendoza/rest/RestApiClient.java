package com.mendoza.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mendoza on 07/07/2016.
 */
public class RestApiClient {
    
    static final String urlBase = "http://localhost:88/services";

    public static JSONArray get(String url, String id) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet getRequest;
            if(id != null){
                getRequest = new HttpGet(urlBase + "/"+ url + ".php" + "?id=" + id);
            }else{
                getRequest = new HttpGet(urlBase + "/"+ url + ".php");
            }
            getRequest.addHeader("accept", "application/json");

            HttpResponse response = httpClient.execute(getRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            String salida = "";
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                salida = salida + output;
            }

            httpClient.getConnectionManager().shutdown();
            JSONObject out = new JSONObject(salida);
            if (out.has("status") && out.getString("status").equals("success")) {
                return out.getJSONArray("data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean post(String url, JSONObject jsondatos) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(urlBase + "/" + url + ".php");
            postRequest.setHeader("content-type", "application/json");
            String datos = jsondatos.toString();
            StringEntity input = new StringEntity(datos, "UTF-8");
            //input.setContentType("application/json");
            postRequest.setEntity(input);

            HttpResponse response = httpClient.execute(postRequest);

            if (response.getStatusLine().getStatusCode() >= 300) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            String salida = "";
            while ((output = br.readLine()) != null) {
                System.err.println(output);
                salida = salida + output;
            }

            httpClient.getConnectionManager().shutdown();
            JSONObject out = new JSONObject(salida);
            if (out.has("status") && out.getString("status").equals("success")) {
                jsondatos = out.getJSONObject("data");
                return true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException ex) {
            Logger.getLogger(RestApiClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean put(String url, int id, JSONObject jsondatos) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPut putRequest = new HttpPut(urlBase + "/" + url + ".php");
            putRequest.setHeader("content-type", "application/json");
            String datos = jsondatos.toString();
            StringEntity input = new StringEntity(datos, "UTF-8");
            //input.setContentType("application/json");
            putRequest.setEntity(input);

            HttpResponse response = httpClient.execute(putRequest);

            if (response.getStatusLine().getStatusCode() >= 300) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            String salida = "";
            while ((output = br.readLine()) != null) {
                System.err.println(output);
                salida = salida + output;
            }

            httpClient.getConnectionManager().shutdown();

            JSONObject out = new JSONObject(salida);
            if (out.has("status") && out.getString("status").equals("success")) {
                jsondatos = out.getJSONObject("data");
                return true;
            }
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException ex) {
            Logger.getLogger(RestApiClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean delete(String url, int id) {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpDelete request = new HttpDelete(urlBase + "/" + url + ".php" + "?id=" + id);
            request.setHeader("content-type", "application/json");

            HttpResponse response = httpClient.execute(request);

            if (response.getStatusLine().getStatusCode() >= 300) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            System.out.println("Output from Server .... \n");
            String salida = "";
            while ((output = br.readLine()) != null) {
                System.err.println(output);
                salida = salida + output;
            }

            httpClient.getConnectionManager().shutdown();
            JSONObject out = new JSONObject(salida);
            if (out.has("status") && out.getString("status").equals("success")) {
                return true;
            }
            
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
    public static void main(String[] args) {
        JSONObject json = new JSONObject();
        try {
            json.put("id", 3);
            json.put("estado", 0);
        } catch (JSONException ex) {
            Logger.getLogger(RestApiClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        put("dominio/", 3, json);
    }
}
