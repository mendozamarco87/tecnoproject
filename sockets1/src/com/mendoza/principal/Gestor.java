package com.mendoza.principal;

import com.mendoza.entidades.Ayuda;
import com.mendoza.entidades.CopiaSeguridad;
import com.mendoza.entidades.Dominio;
import com.mendoza.entidades.Estadistica;
import com.mendoza.entidades.Mail;
import com.mendoza.entidades.Reporte;
import com.mendoza.entidades.ZonaDirecta;
import com.mendoza.entidades.ZonaInversa;
import com.mendoza.interfaces.ListenerMail;
import com.mendoza.javamail.Correo;
import com.mendoza.javamail.Enviar;
import com.mendoza.rest.RestApiClient;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mendoza on 07/07/2016.
 */
public class Gestor {
    static HashMap<String,ListenerMail> ListaAcciones;
    static Enviar enviar = new Enviar();
    
    public Gestor() {
        ListaAcciones = new HashMap<String, ListenerMail>();
        ListaAcciones.put("ayuda", new Ayuda());
        ListaAcciones.put("dominio", new Dominio());
        ListaAcciones.put("zonadirecta", new ZonaDirecta());
        ListaAcciones.put("zonainversa", new ZonaInversa());
        ListaAcciones.put("copiaseguridad", new CopiaSeguridad());
        ListaAcciones.put("reporte", new Reporte());
        ListaAcciones.put("estadistica", new Estadistica());
    }

    public static void dispatchOnReceive(Mail mail) {
        String aux = mail.getAsunto();
        if (aux == null || aux == "")
            return;
        String[] a = aux.split("_");
        if (a.length == 2) {
            getAccion(a[1]).Receive(mail, a[0]);
        } else {
            ListaAcciones.get("ayuda").Receive(mail, "");
        }
    }
    
    public static void dispatchSend(Mail mail) {
        try {
            enviar.enviarCorreo(mail.getAsunto() ,mail.getMensaje(), mail.getCliente());
        } catch (MessagingException ex) {
            Logger.getLogger(Gestor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static ListenerMail getAccion(String name) {
        ListenerMail accion = ListaAcciones.get(name);
        if(accion == null)
            return ListaAcciones.get("ayuda");
        return accion;
    }
    
    public static void main(String[] args) {
        Gestor g = new Gestor();
        JSONObject json = new JSONObject();
        try {
            json.put("id", 3);
            json.put("id_dominio", 3);
            json.put("ip", "127.0.0.3");
            json.put("estado", "1");
            json.put("dominio", "dominio1.com");
        } catch (JSONException ex) {
            Logger.getLogger(RestApiClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        Mail mail = new Mail(5, "grupo12sc@virtual.fcet.uagrm.edu.bo", "2016-07-08", "get_reporte", json.toString());
        Gestor.dispatchOnReceive(mail);
    }
}
