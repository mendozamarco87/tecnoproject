/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mendoza.archivos;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @author TOSHIBA
 */
public class Archivos {

    public void zonaInversa(String P1, String P2, String P3, String dominio, String directorio, String archivo) throws IOException {
        int id = 0;///identificador de archivo
        String encabezado = "$TTL\t604800" + "\n"
                + "@\tIN\tSOA\tdns." + dominio + ". root." + dominio + ". (" + "\n"
                + "\t\t\t015110501\t;Serial" + "\n"
                + "\t\t\t3600\t\t;Refresh" + "\n"
                + "\t\t\t1800\t\t;Retry" + "\n"
                + "\t\t\t604800\t;Expire" + "\n"
                + "\t\t\t86400 )\t;Minimum TTL" + "\n"
                + "\tIN\tNS\tdns." + dominio + ".\n"
                + "\tIN\tPTR\t" + dominio + ".\n";
        String conte = P1 + "\tIN\t" + P2 + "\t" + dominio;
        File arch = new File(directorio + archivo);
        if (arch.exists()) {/// verifica si existe el archivo
            id = 1;
        }
        FileWriter escribirArchivo = new FileWriter(arch, true);
        BufferedWriter buffer = new BufferedWriter(escribirArchivo);
        if (id == 0) {/// si no existe el fichero. netonces insertara el encabezado en el nuevo archivo
            buffer.write(encabezado);
        }
        buffer.write(conte);
        buffer.newLine();
        buffer.close();
    }

    public void zonaDirecta(String P1, String P2, String P3, String dominio, String directorio, String archivo) throws IOException {
        int id = 0;///identificador de archivo
        String encabezado = "$TTL\t604800" + "\n"
                + "@\tIN\tSOA\tdns." + dominio + ". root." + dominio + ". (" + "\n"
                + "\t\t\t015110501\t;Serial" + "\n"
                + "\t\t\t3600\t\t;Refresh" + "\n"
                + "\t\t\t1800\t\t;Retry" + "\n"
                + "\t\t\t604800\t;Expire" + "\n"
                + "\t\t\t86400 )\t;Minimum TTL" + "\n"
                + "@\tIN\tNS\tdns." + dominio + ".\n";

        String conte = P1 + "\tIN" + "\t" + P2 + "\t" + P3;
        File arch = new File(directorio + archivo);
        if (arch.exists()) {/// verifica si existe el archivo
            id = 1;
        }
        FileWriter escribirArchivo = new FileWriter(arch, true);
        BufferedWriter buffer = new BufferedWriter(escribirArchivo);
        if (id == 0) {/// si no existe el fichero. netonces insertara el encabezado en el nuevo archivo
            buffer.write(encabezado);
        }
        buffer.write(conte);
        buffer.newLine();
        buffer.close();
    }
}
