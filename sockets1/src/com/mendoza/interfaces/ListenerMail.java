package com.mendoza.interfaces;

import com.mendoza.entidades.Mail;

import java.util.EventListener;

/**
 * Created by mendoza on 07/07/2016.
 */
public interface ListenerMail extends EventListener {
    void Receive(Mail mail, String accion);
    void Send(Mail mail);
}
