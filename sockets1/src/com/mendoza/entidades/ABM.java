/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import com.mendoza.interfaces.ListenerMail;
import com.mendoza.principal.Gestor;
import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mendo
 */
public abstract class ABM  implements ListenerMail{
    
    int id;
    
    public void registrar(Mail mail) {
        rellenar(mail);
        JSONObject json = getJson();
        if (RestApiClient.post(getNombreServicio(), json)) {
            System.err.println("Se  registro Correctamente...\n" + json.toString());
            mail.setMensaje("Se  registro Correctamente...\n" + json.toString());
            Send(mail);
        } else {
            enviarError(mail);
        }
    }
    
    public void modificar(Mail mail) {
        rellenar(mail);
        JSONObject json = getJson();
        if (RestApiClient.put(getNombreServicio(), this.id, json)) {
            System.err.println("Se actualizo Correctamente...\n" + json.toString());
            mail.setMensaje("Se actualizo Correctamente...\n" + json.toString());
            Send(mail);
        } else {
            enviarError(mail);
        }
    }
    
    public void eliminar(Mail mail) {
        rellenar(mail);
        if (RestApiClient.delete(getNombreServicio(), this.id)) {
            System.err.println(this.id + " Se elimino Correctamente...\n");
            mail.setMensaje(this.id + " Se elimino Correctamente...\n");
            Send(mail);
        } else {
            enviarError(mail);
        }
    }
    
    @Override
    public void Receive(Mail mail, String accion){
        switch (accion) {
            case "registrar":
                registrar(mail);
                break;
            case "modificar":
                modificar(mail);
                break;
            case "eliminar":
                eliminar(mail);
                break;
        }
    }
    
    @Override
    public void Send(Mail mail) {
        Gestor.dispatchSend(mail);
    }
    
    public void rellenar(Mail mail) {
        try {
            JSONObject json = new JSONObject(mail.getMensaje());
            setJson(json);
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void enviarError(Mail mail){
        mail.setMensaje("Ocurrio un error. Intentalo nuevamente...");
    }
    public abstract JSONObject getJson();
    public abstract void setJson(JSONObject json);
    public abstract String getNombreServicio();
}
