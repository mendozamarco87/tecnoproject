/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import com.mendoza.interfaces.ListenerMail;
import com.mendoza.principal.Gestor;
import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mendo
 */
public class Dominio extends ABM{
    String nombre;
    int estado;

    public Dominio() {
    }

    public Dominio(int id, String nombre, int estado) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int isEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("id", this.id);
            json.put("nombre", this.nombre);
            json.put("estado", this.estado);
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    @Override
    public void setJson(JSONObject json) {
        try {
            this.id = json.has("id")? json.getInt("id"): 0;
            this.nombre = json.has("nombre")? json.getString("nombre"): "";
            this.estado = json.has("estado")? json.getInt("estado"): 1;
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getNombreServicio() {
        return "dominio";
    }
        
}
