package com.mendoza.entidades;

import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mendoza on 07/07/2016.
 */
public class Mail {
    int numero;
    String cliente;
    String fecha;
    String asunto;
    String mensaje;

    public Mail() {
    }

    public Mail(int numero, String cliente, String fecha, String asunto, String mensaje) {
        this.numero = numero;
        this.cliente = cliente;
        this.fecha = fecha;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "numero=" + numero +
                ", cliente='" + cliente + '\'' +
                ", fecha='" + fecha + '\'' +
                ", asunto='" + asunto + '\'' +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }

    public JSONObject getJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("numero", this.numero);
            json.put("cliente", this.cliente);
            json.put("fecha", this.fecha);
            json.put("asunto", this.asunto);
            json.put("mensaje", this.mensaje);
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    public void setJson(JSONObject json) {
        try {
            this.numero = json.has("numero")? json.getInt("numero"): 0;
            this.cliente = json.has("cliente")? json.getString("cliente"): "";
            this.fecha = json.has("fecha")? json.getString("fecha"): "";
            this.asunto = json.has("asunto")? json.getString("asunto"): "";
            this.mensaje = json.has("mensaje")? json.getString("mensaje"): "";
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getNombreServicio() {
        return "mail";
    }
    
    public void registrar() {
        JSONObject json = getJson();
        if (RestApiClient.post(getNombreServicio(), json)) {
            System.err.println("Se  registro Correctamente...\n" + json.toString());
        } else {
            System.err.println("No se registro Correctamente el correo");
        }
    }
}
