/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import com.mendoza.interfaces.ListenerMail;
import com.mendoza.principal.Gestor;

/**
 *
 * @author mendo
 */
public class Ayuda implements ListenerMail{

    @Override
    public void Receive(Mail mail, String accion) {
        mail.setAsunto("Ayuda");
        String mensajeAyuda = "Lo sentimos el formato no era el correcto.<br>Debe especificar en el Asunto el caso de uso que desea y enviar los parametros en el mesaje en formato JSON{}";
        mensajeAyuda += "<br>Las operaciones que puede usar son (insertar, modificar, eliminar):";
        mensajeAyuda += "<br>  * ABM Zona directa: operacion_zonadirecta {'id_dominio':'%', 'ip':'%', 'correo_responsable':'%'}";
        mensajeAyuda += "<br>  * ABM Zona inversa: operacion_zonainversa {'id_dominio':'%', 'host':'%'}";
        mensajeAyuda += "<br>  * ABM Zona dominio: operacion_dominio {'nombre':'nombreDelDominio'}";
        mensajeAyuda += "<br>  * ABM Zona permisos: operacion_permisos {'id_dominio':'%', 'ip':'%'}";
        mensajeAyuda += "<br>  * Zona Copia de seguridad: get_copiaseguridad";
        mensajeAyuda += "<br>  * Reportes de Dominios: get_reporte";
        mail.setMensaje(mensajeAyuda);
        Send(mail);
    }

    @Override
    public void Send(Mail mail) {
        Gestor.dispatchSend(mail);
    }
    
}
