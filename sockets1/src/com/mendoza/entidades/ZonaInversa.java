/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mendo
 */
public class ZonaInversa extends ABM {
    int id_dominio;
    String host;
    int estado;

    public ZonaInversa() {
    }

    public ZonaInversa(int id, int idDominio, String hostName, int estado) {
        this.id = id;
        this.id_dominio = idDominio;
        this.host = hostName;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDominio() {
        return id_dominio;
    }

    public void setIdDominio(int idDominio) {
        this.id_dominio = idDominio;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String hostName) {
        this.host = hostName;
    }

    public int isEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("id", this.id);
            json.put("id_dominio", this.id_dominio);
            json.put("host", this.host);
            json.put("estado", this.estado);
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    @Override
    public void setJson(JSONObject json) {
        try {
            this.id = json.has("id")? json.getInt("id"): 0;
            this.id_dominio = json.has("id_dominio")? json.getInt("id_dominio"): 0;
            this.host = json.has("host")? json.getString("host_name"): "";
            this.estado = json.has("estado")? json.getInt("estado"): 1;
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getNombreServicio() {
        return "zonainversa";
    }
    
}
