/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import com.mendoza.archivos.Archivos;
import com.mendoza.interfaces.ListenerMail;
import com.mendoza.javamail.Enviar;
import com.mendoza.principal.Gestor;
import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mendo
 */
public class CopiaSeguridad implements ListenerMail{

    static Enviar enviar = new Enviar();
    String directorio = "C:\\Users\\mendo\\Desktop\\";
    String nombreArchivoZD = "";
    String nombreArchivoZI = "";
    
    @Override
    public void Receive(Mail mail, String accion) {
        String s = getDominio(mail);
        if (!s.isEmpty()) {
            JSONArray j = RestApiClient.get("copiaseguridad", s);
            if (j != null){
                if (generarArchivo(j)) {
                    Send(mail);
                    System.err.println(directorio + nombreArchivoZD);
                } else {
                    enviarError(mail);
                }
            } else {
                enviarError(mail);
            }
        } else {
            enviarError(mail);
        }
    }

    @Override
    public void Send(Mail mail) {
        try {
            enviar.enviarCorreo(mail.getAsunto() ,mail.getMensaje(), mail.getCliente(), directorio + nombreArchivoZD, directorio + nombreArchivoZI);
        } catch (Exception ex) {
            Logger.getLogger(Gestor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void enviarError(Mail mail){
        mail.setMensaje("Ocurrio un error. Intentalo nuevamente...");
        Gestor.dispatchSend(mail);
    }
    
    public String getDominio(Mail mail) {
        try {
            JSONObject json = new JSONObject(mail.getMensaje());
            return json.has("dominio")? json.getString("dominio"): "";
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    private boolean generarArchivo(JSONArray j){
        try{
            JSONObject o = j.getJSONObject(0);
            Archivos a = new Archivos();
            nombreArchivoZD = System.currentTimeMillis() + o.getString("nombre") + ".zone";
            nombreArchivoZI = System.currentTimeMillis() + o.getString("ip") + ".db";
            a.zonaDirecta("dns", "NS", o.getString("ip"), o.getString("nombre"), directorio, nombreArchivoZD);
            String[] e = o.getString("ip").replace('.', ':').split(":");
            a.zonaInversa(e[3], "PTR", o.getString("ip"), o.getString("nombre"), directorio, nombreArchivoZI);
            return true;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    
}
