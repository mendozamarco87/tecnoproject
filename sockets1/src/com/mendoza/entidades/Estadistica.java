/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import static com.mendoza.entidades.Reporte.enviar;
import com.mendoza.interfaces.ListenerMail;
import com.mendoza.javamail.Enviar;
import com.mendoza.principal.Gestor;
import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
/**
 *
 * @author mendo
 */
public class Estadistica implements ListenerMail{
    
    static Enviar enviar = new Enviar();
    
    public Estadistica(){}

    @Override
    public void Receive(Mail mail, String accion) {
        JSONArray output = RestApiClient.get("estadistica", null);
        if(output != null){
            mail.setMensaje(output.toString());
            Send(mail);
        }
    }

    @Override
    public void Send(Mail mail) {
        try{
            enviar.enviarCorreo("Estadisticas de Dominio", mail.mensaje, mail.cliente);
        }catch(Exception ex){
            Logger.getLogger(Gestor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
