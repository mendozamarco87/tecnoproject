/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mendoza.entidades;

import com.mendoza.interfaces.ListenerMail;
import com.mendoza.principal.Gestor;
import com.mendoza.rest.RestApiClient;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author mendo
 */
public class ZonaDirecta extends ABM{
    int id_dominio;
    String ip;
    String correo_responsable;
    int estado;

    public ZonaDirecta() {
    }

    public ZonaDirecta(int id, int idDominio, String ip, String correo_responsable, int estado) {
        this.id = id;
        this.id_dominio = idDominio;
        this.ip = ip;
        this.correo_responsable = correo_responsable;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDominio() {
        return id_dominio;
    }

    public void setIdDominio(int idDominio) {
        this.id_dominio = idDominio;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCorreo_responsable() {
        return this.correo_responsable;
    }

    public void setCorreo_responsable(String c) {
        this.correo_responsable = c;
    }

    public int isEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public JSONObject getJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("id", this.id);
            json.put("id_dominio", this.id_dominio);
            json.put("ip", this.ip);
            json.put("correo_responsable", this.correo_responsable);
            json.put("estado", this.estado);
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return json;
    }

    @Override
    public void setJson(JSONObject json) {
        try {
            this.id = json.has("id")? json.getInt("id"): 0;
            this.id_dominio = json.has("id_dominio")? json.getInt("id_dominio"): 0;
            this.ip = json.has("ip")? json.getString("ip"): "";
            this.correo_responsable = json.has("correo_responsable")? json.getString("correo_responsable"): "";
            this.estado = json.has("estado")? json.getInt("estado"): 1;
        } catch (JSONException ex) {
            Logger.getLogger(Dominio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getNombreServicio() {
        return "zonadirecta";
    }
    
}
