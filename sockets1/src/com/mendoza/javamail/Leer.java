/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mendoza.javamail;

import com.mendoza.Constantes;
import com.mendoza.entidades.Mail;
import com.mendoza.principal.Gestor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

/**
 * @author jose mejia
 */
public class Leer {
    public static int controller = 7;
    
    private Store almacenMensaje;
    private Folder bandejaEntrada;

    public void Conectar() {
        String servidor = "mail.ficct.uagrm.edu.bo";
        String cuenta = "grupo12sc";
        String clave = "grupo12grupo12";
        Properties propiedadesLectura = System.getProperties();
        Session sesionLectura = Session.getInstance(propiedadesLectura);
        try {
            almacenMensaje = sesionLectura.getStore("pop3");
            //almacenMensaje.connect(servidor, cuenta, clave);
            almacenMensaje.connect(Constantes.SERVER,
                    Constantes.CORREO_SISTEMA, Constantes.PASS_SISTEMA);

            bandejaEntrada = almacenMensaje.getFolder("inbox");
            if ((bandejaEntrada != null) && (bandejaEntrada.exists())) {
                leerCorreo();
                System.out.println("Esperando para leer :3");
            }
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(Leer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(Leer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void leerCorreo() {
        Message mensajes[];
        try {
            bandejaEntrada.open(Folder.READ_WRITE);
        } catch (MessagingException ex) {
            Logger.getLogger(Leer.class.getName()).log(Level.SEVERE, null, ex);
        }
        FlagTerm opcionesLectura = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
        try {
            mensajes = bandejaEntrada.search(opcionesLectura);
            for(int i=controller; i<mensajes.length;i++){
                Message mensaje = mensajes[i];
                Mail m = procesarMensaje(mensaje);
                m.registrar();
                if (!mensaje.getSubject().contains("Respuesta del Servidor")){
                    Gestor.dispatchOnReceive(m);
                }
                mensaje.setFlag(Flags.Flag.DELETED, true);
                System.out.println("");
            }
//            for (Message mensaje : mensajes) {
//                Mail m = procesarMensaje(mensaje);
//                if (!mensaje.getSubject().contains("Respuesta del Servidor")){
//                    Gestor.dispatchOnReceive(m);
//                }
//                m.setMensaje(m.getJson().toString());
//                m.registrar(m);
//                mensaje.setFlag(Flags.Flag.DELETED, true);
//                System.out.println("");
//            }
            bandejaEntrada.close(true);
            almacenMensaje.close();
        } catch (MessagingException ex) {
            Logger.getLogger(Leer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Leer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Mail procesarMensaje(Message mensaje) throws MessagingException, IOException {
        

        String contentType = mensaje.getContentType();
        ArrayList<String> messageContent = new ArrayList<>();
        String attachFiles = "";
        if (contentType.contains("multipart")) {
            // content may contain attachments
            Multipart multiPart = (Multipart) mensaje.getContent();
            int numberOfParts = multiPart.getCount();
            for (int partCount = 0; partCount < numberOfParts; partCount++) {
                MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
                if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                    // this part is attachment
                    String fileName = part.getFileName();
                    //attachFiles += fileName + ", ";
                    //part.saveFile(new File(part.getFileName()));
                    //part.saveFile("/home/alejandro/proyectos/correo/" + File.separator + fileName);
                    System.out.println("El mensaje es un archivo" + fileName);
                } else {
                    //this part may be the message content
                    messageContent.add(part.getContent().toString());
                    System.out.println(">>> " + messageContent);
                }

            }
        } else {
            messageContent.add(mensaje.getContent().toString());
            System.out.println("---> " + messageContent);
        }
        
        Mail m = new Mail();
        m.setNumero(mensaje.getMessageNumber());
        m.setFecha("" + mensaje.getSentDate());
        m.setCliente("" + mensaje.getFrom()[0]);
        m.setAsunto(mensaje.getSubject());
        m.setMensaje(messageContent.isEmpty()? "": messageContent.get(0));
        System.out.println(m.toString());
        return m;
    }
}
