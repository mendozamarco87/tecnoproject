/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mendoza.javamail;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author jose mejia
 */
public class Correo extends Thread {
    boolean terminado = false;
    int intervalo = 5 * 1000;
    Leer leer;

    public Correo() {
        leer = new Leer();
        leer.Conectar();
    }

    @Override
    public void run() {
        super.run();
        while (!terminado) {
            leer.leerCorreo();
            try {
                Thread.sleep(intervalo);
            } catch (InterruptedException ex) {
                Logger.getLogger(Correo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void Escuchar() {
        this.start();
    }

}
