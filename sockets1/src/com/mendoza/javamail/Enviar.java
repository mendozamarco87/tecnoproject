/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mendoza.javamail;

import com.mendoza.Constantes;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author jose mejia
 */
public class Enviar {
    public Session sesionCorreo;
    String correoservidor = Constantes.CORREO_ADMINISTRADOR;
    final String username =  Constantes.CORREO_ADMINISTRADOR;
    final String password = Constantes.PASS_SISTEMA;
    String asunto = "Respuesta del Servidor ";
        
    public void enviarCorreo(String asunto ,String html , String destinatario) throws MessagingException{        
        Properties propiedadesEnvio = System.getProperties();
        propiedadesEnvio.put("mail.smtp.host", Constantes.SERVER);
        propiedadesEnvio.put("mail.smtp.port", "25");
        propiedadesEnvio.put("mail.smtp.auth", "false");
        propiedadesEnvio.put("mail.smtp.starttls.enable", "false");
        sesionCorreo = Session.getDefaultInstance(propiedadesEnvio);
        MimeMessage mensajeEnviar = new MimeMessage(sesionCorreo);
        mensajeEnviar.setFrom(new InternetAddress(correoservidor));
        mensajeEnviar.addRecipient(Message.RecipientType.TO ,
                new InternetAddress(destinatario));
        mensajeEnviar.setSubject(asunto + " " + this.asunto);
        mensajeEnviar.setContent(html,"text/html");
        Transport t = sesionCorreo.getTransport("smtp");
        t.connect();
        t.sendMessage(mensajeEnviar, mensajeEnviar.getAllRecipients());
        t.close();
    }
    
    public void enviarCorreo(String asunto, String html , String destinatario ,String... Nombre) throws MessagingException, IOException{
        Properties propiedadesEnvio = System.getProperties();
        propiedadesEnvio.put("mail.smtp.host", Constantes.SERVER);
        propiedadesEnvio.put("mail.smtp.port", 25);
        propiedadesEnvio.put("mail.smtp.auth", "false");
        propiedadesEnvio.put("mail.smtp.starttls.enable", "false");
        sesionCorreo = Session.getInstance(propiedadesEnvio,
            new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username,password);
                }
            }
        );
        
        MimeBodyPart cuerpodelmensaje = new MimeBodyPart();
        cuerpodelmensaje.setContent(html,"text/html");
        
        Multipart multiparte = new MimeMultipart();
         
        multiparte.addBodyPart(cuerpodelmensaje);
        for (String a : Nombre) {
            MimeBodyPart adjunto = new MimeBodyPart();
            adjunto.attachFile(a);
            multiparte.addBodyPart(adjunto);
        }
        
        Message mensajeEnviar = new MimeMessage(sesionCorreo);
        mensajeEnviar.setFrom(new InternetAddress(correoservidor));
        mensajeEnviar.addRecipient(Message.RecipientType.TO ,
                new InternetAddress(destinatario));
        mensajeEnviar.setSubject(asunto + " " + this.asunto);
        mensajeEnviar.setContent(multiparte);
        Transport.send(mensajeEnviar);
    }
    
    public static void main(String[] args) {
        try {
            Enviar e = new Enviar();
            e.enviarCorreo("prueba","hola hola carambola", "grupo12sc@virtual.fcet.uagrm.edu.bo");
        } catch (MessagingException ex) {
            Logger.getLogger(Enviar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
